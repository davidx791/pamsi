// statki.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include<cstdlib>
#include <conio.h>
#include <ctime>
#include<vector>
#include<string>
#include<windows.h>

using namespace std;

int N = 10;
class pole;
class statek;

char zwrocLitere(int x);
int zwrocCyfre(char x);
void wyswietlStatki(char ** tab);
vector<pole>zapamietaneNIE;
vector<pole>zapamietaneNIE2;
vector<statek> wprowadzStatki(char **tab, vector<pole> losowyWektor, int opcja);
vector<pole> sprawdzStrzal(int x, int y, vector<pole> wektorS);
vector<pole> sprawdz(int x, int y, vector<pole> wektorT);
char ** dodajOtoczke(int x, int y, int pol,int maszt, char **tab,int czyKomp);

class pole
{
public:
	int x,y;
	pole() {};
};
class statek
{
public:
	int x_planszy, y_planszy,kier,rodzaj;
};

char zwrocLitere(int x)
{
	if (x == 0)	return 'A';
	if (x == 1)	return 'B';
	if (x == 2)	return 'C';
	if (x == 3)	return 'D';
	if (x == 4)	return 'E';
	if (x == 5)	return 'F';
	if (x == 6)	return 'G';
	if (x == 7)	return 'H';
	if (x == 8)	return 'I';
	if (x == 9)	return 'J';
	return ' ';
}

int zwrocCyfre(char x)
{
	if (x == 'A' || x == 'a')	return 0;
	if (x == 'B' || x == 'b')	return 1;
	if (x == 'C' || x == 'c')	return 2;
	if (x == 'D' || x == 'd')	return 3;
	if (x == 'E' || x == 'e')	return 4;
	if (x == 'F' || x == 'f')	return 5;
	if (x == 'G' || x == 'g')	return 6;
	if (x == 'H' || x == 'h')	return 7;
	if (x == 'I' || x == 'i')	return 8;
	if (x == 'J' || x == 'j')	return 9;
	return -1;
}

void wyswietlStatki(char ** tab)
{
	cout << "   ";
	for (int i = 0; i < N; i++) cout << setw(3) << i;
	cout << endl;
	for (int i = 0; i < N; i++)
	{
		cout << setw(3) << zwrocLitere(i);
		for (int j = 0; j < N; j++)
			cout << setw(3) << tab[i][j];
		cout << endl;
	}
}

char ** dodajOtoczke(int x, int y, int pol, int maszt, char ** tab, int czyKomp)
{
	bool czyBylo;
	pole tmp;
	if (pol == 1)//*******************PIONOWO**************
	{
		for (int j = 0; j < 3; j++)//ustawienie otoczki
		{
			for (int i = 0; i < maszt + 2; i++)
			{
				if (j == 0)
				{
					if (y != 0)
					{
						if (i == 0) //lewy gorny ogranicznik
						{
							if (x != 0)
							{
								tab[x - 1 + i][y - 1 + j] = 'X';
								if (czyKomp)
								{
									czyBylo = false;
									tmp.x = x - 1 + i;
									tmp.y = y - 1 + j;
									for (int k = 0; k < zapamietaneNIE.size(); k++)
										if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
										{
											czyBylo = true;
											break;
										}
									if (!czyBylo)
										zapamietaneNIE.push_back(tmp);
								}
							}
						}
						else
						{
							if (i == maszt + 2 - 1) //lewy dolny ogranicznik
							{
								if (x != N - maszt)
								{
									tab[x - 1 + i][y - 1 + j] = 'X';
									if (czyKomp)
									{
										czyBylo = false;
										tmp.x = x - 1 + i;
										tmp.y = y - 1 + j;
										for (int k = 0; k < zapamietaneNIE.size(); k++)
											if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
											{
												czyBylo = true;
												break;
											}
										if (!czyBylo)
											zapamietaneNIE.push_back(tmp);
									}
								}
							}
							else // i= 1,2,3,...  maszt+2-1 lewy ogranicznik
							{
								tab[x - 1 + i][y - 1 + j] = 'X';
								if (czyKomp)
								{
									czyBylo = false;
									tmp.x = x - 1 + i;
									tmp.y = y - 1 + j;
									for (int k = 0; k < zapamietaneNIE.size(); k++)
										if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
										{
											czyBylo = true;
											break;
										}
									if (!czyBylo)
										zapamietaneNIE.push_back(tmp);
								}
							}
						}
					}
				}
				else
				{
					if (j == 2)
					{
						if (y != 9)
						{
							if (i == 0) //prawy gorny ogranicznik
							{
								if (x != 0)
								{
									tab[x - 1 + i][y - 1 + j] = 'X';
									if (czyKomp)
									{
										czyBylo = false;
										tmp.x = x - 1 + i;
										tmp.y = y - 1 + j;
										for (int k = 0; k < zapamietaneNIE.size(); k++)
											if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
											{
												czyBylo = true;
												break;
											}
										if (!czyBylo)
											zapamietaneNIE.push_back(tmp);
									}
								}
							}
							else
							{
								if (i == maszt + 2 - 1) //prawy dolny ogranicznik
								{
									if (x != N - maszt)
									{
										tab[x - 1 + i][y - 1 + j] = 'X';
										if (czyKomp)
										{
											czyBylo = false;
											tmp.x = x - 1 + i;
											tmp.y = y - 1 + j;
											for (int k = 0; k < zapamietaneNIE.size(); k++)
												if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
												{
													czyBylo = true;
													break;
												}
											if (!czyBylo)
												zapamietaneNIE.push_back(tmp);
										}
									}
								}
								else // i= 1,2,3,... maszt + 2 - 1 prawy ogranicznik
								{
									tab[x + i - 1][y - 1 + j] = 'X';
									if (czyKomp)
									{
										czyBylo = false;
										tmp.x = x - 1 + i;
										tmp.y = y - 1 + j;
										for (int k = 0; k < zapamietaneNIE.size(); k++)
											if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
											{
												czyBylo = true;
												break;
											}
										if (!czyBylo)
											zapamietaneNIE.push_back(tmp);
									}
								}
							}
						}
					}
					else //j==1
					{
						if (i == 0) //gorny ogranicznik
						{
							if (x != 0)
							{
								tab[x - 1 + i][y + j - 1] = 'X';
								if (czyKomp)
								{
									czyBylo = false;
									tmp.x = x - 1 + i;
									tmp.y = y - 1 + j;
									for (int k = 0; k < zapamietaneNIE.size(); k++)
										if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
										{
											czyBylo = true;
											break;
										}
									if (!czyBylo)
										zapamietaneNIE.push_back(tmp);
								}
							}
						}
						if (i == maszt + 2 - 1) //dolny ogranicznik
						{
							if (x != N - maszt)
							{
								tab[x - 1 + i][y + j - 1] = 'X';
								if (czyKomp)
								{
									czyBylo = false;
									tmp.x = x - 1 + i;
									tmp.y = y - 1 + j;
									for (int k = 0; k < zapamietaneNIE.size(); k++)
										if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
										{
											czyBylo = true;
											break;
										}
									if (!czyBylo)
										zapamietaneNIE.push_back(tmp);
								}
							}
						}
					}
				}
			}
		}
	}
	else//*******************POZIOMO**************
	{
		for (int i = 0; i < 3; i++) //ustawienie otoczki
		{
			for (int j = 0; j < maszt + 2; j++)
			{
				if (i == 0)
				{
					if (x != 0)
					{
						if (j == 0) //lewy g�rny ogranicznik
						{
							if (y != 0)
							{
								tab[x - 1 + i][y + j - 1] = 'X';
								if (czyKomp)
								{
									czyBylo = false;
									tmp.x = x - 1 + i;
									tmp.y = y - 1 + j;
									for (int k = 0; k < zapamietaneNIE.size(); k++)
										if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
										{
											czyBylo = true;
											break;
										}
									if (!czyBylo)
										zapamietaneNIE.push_back(tmp);
								}
							}
						}
						else
						{
							if (j == maszt + 2 - 1) //prawy g�rny ogranicznik
							{
								if (y != N - maszt)
								{
									tab[x - 1 + i][y + j - 1] = 'X';
									if (czyKomp)
									{
										czyBylo = false;
										tmp.x = x - 1 + i;
										tmp.y = y - 1 + j;
										for (int k = 0; k < zapamietaneNIE.size(); k++)
											if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
											{
												czyBylo = true;
												break;
											}
										if (!czyBylo)
											zapamietaneNIE.push_back(tmp);
									}
								}
							}
							else // j=  1,2,3,... maszt+2-1  g�rny ogranicznik
							{
								tab[x - 1 + i][y + j - 1] = 'X';
								if (czyKomp)
								{
									czyBylo = false;
									tmp.x = x - 1 + i;
									tmp.y = y - 1 + j;
									for (int k = 0; k < zapamietaneNIE.size(); k++)
										if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
										{
											czyBylo = true;
											break;
										}
									if (!czyBylo)
										zapamietaneNIE.push_back(tmp);
								}
							}
						}
					}
				}
				else
				{
					if (i == 2)
					{
						if (x != 9)
						{
							if (j == 0) //lewy dolny ogranicznik
							{
								if (y != 0)
								{
									tab[x + i - 1][y + j - 1] = 'X';
									if (czyKomp)
									{
										czyBylo = false;
										tmp.x = x - 1 + i;
										tmp.y = y - 1 + j;
										for (int k = 0; k < zapamietaneNIE.size(); k++)
											if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
											{
												czyBylo = true;
												break;
											}
										if (!czyBylo)
											zapamietaneNIE.push_back(tmp);
									}
								}
							}
							else
							{
								if (j == maszt + 2 - 1) //prawy dolny ogranicznik
								{
									if (y != N - maszt)
									{
										tab[x + i - 1][y + j - 1] = 'X';
										if (czyKomp)
										{
											czyBylo = false;
											tmp.x = x - 1 + i;
											tmp.y = y - 1 + j;
											for (int k = 0; k < zapamietaneNIE.size(); k++)
												if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
												{
													czyBylo = true;
													break;
												}
											if (!czyBylo)
												zapamietaneNIE.push_back(tmp);
										}
									}
								}
								else // j=  1,2,3,... maszt+2-1 dolny ogranicznik
								{
									tab[x + i - 1][y + j - 1] = 'X';
									if (czyKomp)
									{
										czyBylo = false;
										tmp.x = x - 1 + i;
										tmp.y = y - 1 + j;
										for (int k = 0; k < zapamietaneNIE.size(); k++)
											if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
											{
												czyBylo = true;
												break;
											}
										if (!czyBylo)
											zapamietaneNIE.push_back(tmp);
									}
								}
							}
						}
					}
					else //i==1
					{
						if (j == 0) //lewy ogranicznik
						{
							if (y != 0)
							{
								tab[x + i - 1][y + j - 1] = 'X';
								if (czyKomp)
								{
									czyBylo = false;
									tmp.x = x - 1 + i;
									tmp.y = y - 1 + j;
									for (int k = 0; k < zapamietaneNIE.size(); k++)
										if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
										{
											czyBylo = true;
											break;
										}
									if (!czyBylo)
										zapamietaneNIE.push_back(tmp);
								}
							}
						}
						if (j == maszt + 2 - 1) //prawy ogranicznik
						{
							if (y != N - maszt)
							{
								tab[x + i - 1][y + j - 1] = 'X';
								if (czyKomp)
								{
									czyBylo = false;
									tmp.x = x - 1 + i;
									tmp.y = y - 1 + j;
									for (int k = 0; k < zapamietaneNIE.size(); k++)
										if (zapamietaneNIE[k].x == tmp.x && zapamietaneNIE[k].y == tmp.y)
										{
											czyBylo = true;
											break;
										}
									if (!czyBylo)
										zapamietaneNIE.push_back(tmp);
								}
							}
						}
					}
				}
			}
		}
	}
	return tab;
}

vector<pole> sprawdz(int x, int y, vector<pole> wektorT)
{
	vector<pole> tmp;
	for (int i = 0; i < wektorT.size(); i++)
		if (!(x == wektorT[i].x && y == wektorT[i].y))
			tmp.push_back(wektorT[i]);
	return tmp;
}

vector<pole> sprawdzStrzal(int x, int y, vector<pole> wektorS)
{
	vector<pole> tmp;
	bool czyBylo=true;
	for (int i = 0; i < wektorS.size(); i++)
		if (!(x == wektorS[i].x && y == wektorS[i].y))
			tmp.push_back(wektorS[i]);
	return tmp;
}

vector<statek> wprowadzStatki(char **tab, vector<pole> losowyWektor,int opcja)
{
	vector<statek> statki;
	statek nowystatek;
	pole tmp;
	char wsp_x;
	int wsp_y, kierunek, los;
	bool czyPoprawnie;
	for (int maszt = 4; maszt > 0; maszt--)
	{
		for (int mnoznik = 0; mnoznik < (5 - maszt); mnoznik++)
		{
			czyPoprawnie = true;
			while (czyPoprawnie)
			{
				czyPoprawnie = false;
				if (opcja == 0)
				{
					cout << endl << "Podaj poczatek " << maszt << "-masztowca: " << endl; //wprowadzanie statkow
					cin >> wsp_x;
					cin >> wsp_y;
					if (maszt > 1)
					{
						cout << "Wybierz polozenie: 0 - poziomo / 1 - pionowo " << endl;
						cin >> kierunek;
					}
					if (kierunek == 0) //kierunek poziomo
						if (!(zwrocCyfre(wsp_x) > -1 && zwrocCyfre(wsp_x) < 10 && wsp_y > -1 && wsp_y < (N - maszt + 1)))
							czyPoprawnie = true;
					else //kierunek - pionowo
						if (!(zwrocCyfre(wsp_x) > -1 && zwrocCyfre(wsp_x) < (N - maszt + 1) && wsp_y > -1 && wsp_y < 10))
							czyPoprawnie = true;
				}
				else
				{
					//losowanie
					if (maszt > 1)
						kierunek = rand() % 2;
					else
						kierunek = 0;
					if (kierunek == 0)
					{
						los = rand() % N;
						wsp_x = zwrocLitere(los);
						wsp_y = rand() % (N - maszt + 1);
					}
					else
					{
						los = rand() % (N - maszt + 1);
						wsp_x = zwrocLitere(los);
						wsp_y = rand() % N;
					}
				}
				if (!czyPoprawnie)//*******************PIONOWO**************
				{
					if (kierunek == 1)
					{
						for (int i = 0; i < maszt; i++) //sprawdzenie pustych p�l
							if (tab[zwrocCyfre(wsp_x) + i][wsp_y] == 'S' || tab[zwrocCyfre(wsp_x) + i][wsp_y] == '/')
							{
								czyPoprawnie = true;
								break;
							}
						if (!czyPoprawnie)
						{
							if (losowyWektor.size()) //sprawdzanie wektora
								for (int i = 0; i < maszt; i++)
								{
									for (int j = 0; j < losowyWektor.size(); j++)
										if (losowyWektor[j].x == zwrocCyfre(wsp_x) + i && losowyWektor[j].y == wsp_y)
										{
											czyPoprawnie = true;
											break;
										}
									if (czyPoprawnie)
										break;
								}
						}
						if (!czyPoprawnie)
						{
							for (int i = 0; i < maszt; i++) //przypisanie symbolu statku
							{
								tab[zwrocCyfre(wsp_x) + i][wsp_y] = 'S';
								tmp.x = zwrocCyfre(wsp_x) + i;
								tmp.y = wsp_y;
								losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
							}
							for (int j = 0; j < 3; j++)//ustawienie otoczki
							{
								for (int i = 0; i < maszt + 2; i++)
								{
									if (j == 0)
									{
										if (wsp_y != 0)
										{
											if (i == 0) //lewy gorny ogranicznik
											{
												if (zwrocCyfre(wsp_x) != 0)
												{
													tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y - 1 + j] = '/';
													tmp.x = zwrocCyfre(wsp_x) - 1 + i;
													tmp.y = wsp_y - 1 + j;
													losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
												}
											}
											else
											{
												if (i == maszt + 2 - 1) //lewy dolny ogranicznik
												{
													if (zwrocCyfre(wsp_x) != N - maszt)
													{
														tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y - 1 + j] = '/';
														tmp.x = zwrocCyfre(wsp_x) - 1 + i;
														tmp.y = wsp_y - 1 + j;
														losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
													}
												}
												else // i= 1,2,3,...  maszt+2-1 lewy ogranicznik
												{
													tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y - 1 + j] = '/';
													tmp.x = zwrocCyfre(wsp_x) - 1 + i;
													tmp.y = wsp_y - 1 + j;
													losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
												}
											}
										}
									}
									else
									{
										if (j == 2)
										{
											if (wsp_y != 9)
											{
												if (i == 0) //prawy gorny ogranicznik
												{
													if (zwrocCyfre(wsp_x) != 0)
													{
														tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y - 1 + j] = '/';
														tmp.x = zwrocCyfre(wsp_x) - 1 + i;
														tmp.y = wsp_y - 1 + j;
														losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
													}
												}
												else
												{
													if (i == maszt + 2 - 1) //prawy dolny ogranicznik
													{
														if (zwrocCyfre(wsp_x) != N - maszt)
														{
															tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y - 1 + j] = '/';
															tmp.x = zwrocCyfre(wsp_x) - 1 + i;
															tmp.y = wsp_y - 1 + j;
															losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
														}
													}
													else // i= 1,2,3,... maszt + 2 - 1 prawy ogranicznik
													{
														tab[zwrocCyfre(wsp_x) + i - 1][wsp_y - 1 + j] = '/';
														tmp.x = zwrocCyfre(wsp_x) + i - 1;
														tmp.y = wsp_y - 1 + j;
														losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
													}
												}
											}
										}
										else //j==1
										{
											if (i == 0) //gorny ogranicznik
											{
												if (zwrocCyfre(wsp_x) != 0)
												{
													tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y + j - 1] = '/';
													tmp.x = zwrocCyfre(wsp_x) - 1 + i;
													tmp.y = wsp_y + j - 1;
													losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
												}
											}
											if (i == maszt + 2 - 1) //dolny ogranicznik
											{
												if (zwrocCyfre(wsp_x) != N - maszt)
												{
													tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y + j - 1] = '/';
													tmp.x = zwrocCyfre(wsp_x) - 1 + i;
													tmp.y = wsp_y + j - 1;
													losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
												}
											}
										}
									}
								}
							}
						}
					}
					else //*******************POZIOMO**************
					{
						for (int i = 0; i < maszt; i++) //sprawdzanie pustych p�l
							if (tab[zwrocCyfre(wsp_x)][wsp_y + i] == 'S' || tab[zwrocCyfre(wsp_x)][wsp_y + i] == '/')
							{
								czyPoprawnie = true;
								break;
							}
						if (!czyPoprawnie)
						{
							if (losowyWektor.size()) //sprawdzanie wektora
							{
								for (int i = 0; i < maszt; i++)
								{
									for (int j = 0; j < losowyWektor.size(); j++)
									{
										if (losowyWektor[j].x == zwrocCyfre(wsp_x) && losowyWektor[j].y == wsp_y + i)
										{
											czyPoprawnie = true;
											break;
										}
									}
									if (czyPoprawnie)
										break;
								}
							}
						}
						if (!czyPoprawnie)
						{
							for (int i = 0; i < maszt; i++) //przypisanie symbolu statku
							{
								tab[zwrocCyfre(wsp_x)][wsp_y + i] = 'S';
								tmp.x = zwrocCyfre(wsp_x);
								tmp.y = wsp_y + i;
								losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
							}
							for (int i = 0; i < 3; i++) //ustawienie otoczki
							{
								for (int j = 0; j < maszt + 2; j++)
								{
									if (i == 0)
									{
										if (zwrocCyfre(wsp_x) != 0)
										{
											if (j == 0) //lewy g�rny ogranicznik
											{
												if (wsp_y != 0)
												{
													tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y + j - 1] = '/';
													tmp.x = zwrocCyfre(wsp_x) - 1 + i;
													tmp.y = wsp_y + j - 1;
													losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
												}
											}
											else
											{
												if (j == maszt + 2 - 1) //prawy g�rny ogranicznik
												{
													if (wsp_y != N - maszt)
													{
														tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y + j - 1] = '/';
														tmp.x = zwrocCyfre(wsp_x) - 1 + i;
														tmp.y = wsp_y + j - 1;
														losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
													}
												}
												else // j=  1,2,3,... maszt+2-1  g�rny ogranicznik
												{
													tab[zwrocCyfre(wsp_x) - 1 + i][wsp_y + j - 1] = '/';
													tmp.x = zwrocCyfre(wsp_x) - 1 + i;
													tmp.y = wsp_y + j - 1;
													losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
												}
											}
										}
									}
									else
									{
										if (i == 2)
										{
											if (zwrocCyfre(wsp_x) != 9)
											{
												if (j == 0) //lewy dolny ogranicznik
												{
													if (wsp_y != 0)
													{
														tab[zwrocCyfre(wsp_x) + i - 1][wsp_y + j - 1] = '/';
														tmp.x = zwrocCyfre(wsp_x) + i - 1;
														tmp.y = wsp_y + j - 1;
														losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
													}
												}
												else
												{
													if (j == maszt + 2 - 1) //prawy dolny ogranicznik
													{
														if (wsp_y != N - maszt)
														{
															tab[zwrocCyfre(wsp_x) + i - 1][wsp_y + j - 1] = '/';
															tmp.x = zwrocCyfre(wsp_x) + i - 1;
															tmp.y = wsp_y + j - 1;
															losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
														}
													}
													else // j=  1,2,3,... maszt+2-1 dolny ogranicznik
													{
														tab[zwrocCyfre(wsp_x) + i - 1][wsp_y + j - 1] = '/';
														tmp.x = zwrocCyfre(wsp_x) + i - 1;
														tmp.y = wsp_y + j - 1;
														losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
													}
												}

											}
										}
										else //i==1
										{
											if (j == 0) //lewy ogranicznik
											{
												if (wsp_y != 0)
												{
													tab[zwrocCyfre(wsp_x) + i - 1][wsp_y + j - 1] = '/';
													tmp.x = zwrocCyfre(wsp_x) + i - 1;
													tmp.y = wsp_y + j - 1;
													losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
												}
											}
											if (j == maszt + 2 - 1) //prawy ogranicznik
											{
												if (wsp_y != N - maszt)
												{
													tab[zwrocCyfre(wsp_x) + i - 1][wsp_y + j - 1] = '/';
													tmp.x = zwrocCyfre(wsp_x) + i - 1;
													tmp.y = wsp_y + j - 1;
													losowyWektor.push_back(tmp); // dodawanie do wyjatkow w vectorze
												}
											}
										}
									}
								}
							}
						}
					}
				}
				if (opcja == 0)
				{
					if (czyPoprawnie)
						cout << "Wprowadz poprawne pole" << endl;
					else
					{
						cout << endl << "MOJE STATKI" << endl;
						wyswietlStatki(tab);
						cout << endl << "Utworzono statek" << endl;
					}
				}
			}
			nowystatek.x_planszy = zwrocCyfre(wsp_x);
			nowystatek.y_planszy = wsp_y;
			nowystatek.kier = kierunek;
			nowystatek.rodzaj = maszt;
			statki.push_back(nowystatek);
		}
	}
	return statki;
}

int main()
{
	int pion = 0, poziom = 0, pion2 = 0, poziom2 = 0, licznik_pom=0, licznik_pom2 = 0,dlugosc, los_1,los_2, spr,spr2, licznik,licznik2, strzal_y, strzal_komp_y, otoczka_y,otoczka_y2, polozenie,polozenie2;
	bool trafiony, trafionyPC;
	char strzal_x, strzal_komp_x,opt,otoczka_x,otoczka_x2;
	vector<statek>MojeStatki;
	vector<statek>StatkiPC;
	vector<pole>wektorTrafionych;
	vector<pole>wektorTrafionychPC;
	vector<pole>wektorStrzelonych;
	vector<pole>wektorStrzelonychPC;
	vector<pole>zapamietane;
	vector<pole>zapamietane2;
	vector <pole> losowyWektor1; //wektor powtorzen 
	vector <pole> losowyWektor2; //wektor powtorzen 
	pole trafionePole;
	srand(time(NULL));

	//utworzenie dwoch plansz do gry
	char ** tab1 = new char *[N];
	char ** tab2 = new char *[N];
	char ** tabpom = new char *[N]; //tablica pom
	for (int i = 0; i < N; i++)
	{
		tab1[i] = new char[N];
		tab2[i] = new char[N];
		tabpom[i] = new char[N];
	}
	for (int i = 0; i < N; i++) // i wypelnienie '-'
		for (int j = 0; j < N; j++)
		{
			tab1[i][j] = '_';
			tab2[i][j] = '_';
			tabpom[i][j] = '_';
		}
	
	//uzupelnianie plansz
	/*cout << "MOJE STATKI" << endl;
	wyswietlStatki(tab1);
	MojeStatki=wprowadzStatki(tab1,losowyWektor1,0);
	cout << "MOJE STATKI" << endl;
	wyswietlStatki(tab1);*/
	StatkiPC=wprowadzStatki(tab2,losowyWektor2,1);
	cout << "STATKI KOMPUTERA" << endl;
	wyswietlStatki(tab2);
	//statki komputera 2
	MojeStatki = wprowadzStatki(tab1, losowyWektor1, 1);
	cout << "STATKI KOMPUTERA" << endl;
	wyswietlStatki(tab1);
	
	//czyszczenie planszyz obramowek
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
		{
			if (tab1[i][j] == '/')
				tab1[i][j] = '_';
			if (tab2[i][j] == '/')
				tab2[i][j] = '_';
		}

	//informacje dotyczace wspolrzednych utworzonych plansz
	/*cout <<endl<< "INFO - STATKI" << endl;
	cout << "MOJE STATKI: " << endl;
	for (int i = 0; i < MojeStatki.size(); i++)
		cout << MojeStatki[i].rodzaj << "  wspolrzedne: " << zwrocLitere(MojeStatki[i].x_planszy)<< MojeStatki[i].y_planszy << " kierunek: " << MojeStatki[i].kier << endl;
	cout <<endl<< "STATKI KOMPUTERA: " << endl;
	for (int i = 0; i < StatkiPC.size(); i++)
		cout << StatkiPC[i].rodzaj << "  wspolrzedne: " << zwrocLitere(StatkiPC[i].x_planszy)<< StatkiPC[i].y_planszy << " kierunek: " << StatkiPC[i].kier << endl;
*/
	//Dodawanie p�l do wektora trafionych
	pole pom;
	for (int i = 0; i < MojeStatki.size(); i++)
		for (int j = 0; j<MojeStatki[i].rodzaj; j++)
		{
			if (MojeStatki[i].kier == 0)
			{
				pom.x = MojeStatki[i].x_planszy;
				pom.y = MojeStatki[i].y_planszy + j;
			}
			else //pionowy
			{
				pom.x = MojeStatki[i].x_planszy + j;
				pom.y = MojeStatki[i].y_planszy;
			}
			wektorTrafionych.push_back(pom);
		}
	for (int i = 0; i < StatkiPC.size(); i++)
		for (int j=0; j<StatkiPC[i].rodzaj;j++)
		{
			if (StatkiPC[i].kier == 0)
			{
				pom.x = StatkiPC[i].x_planszy;
				pom.y = StatkiPC[i].y_planszy+j;
			}
			else //pionowy
			{
				pom.x = StatkiPC[i].x_planszy+j;
				pom.y = StatkiPC[i].y_planszy;
			}
			wektorTrafionychPC.push_back(pom);
		}
		
	//Dodawanie p�l do wektora strzelonych
	pole pom2;
	for (int i = 0; i < N; i++)
		for (int j = 0; j<N; j++)
		{				
			pom2.x = i;
			pom2.y = j;
			wektorStrzelonych.push_back(pom2);
			wektorStrzelonychPC.push_back(pom2);
		}
	cout <<endl<< "NACISNIJ ABY ZACZAC GRE" << endl;
	getch();

	while (true) //GRA
	{
		system("cls");
		do
		{
			trafionyPC = false;
			cout << "KOMPUTER WYKONUJE RUCH: " << endl;
			Sleep(2000);
			if(zapamietane.size()==0) //je�li nie mamy podejrzanych punkt�w
			{
				do
				{
					spr = wektorStrzelonychPC.size();
					los_1 = rand() % wektorStrzelonychPC.size();
					strzal_komp_x = zwrocLitere(wektorStrzelonychPC[los_1].x);
					strzal_komp_y = wektorStrzelonychPC[los_1].y;
					wektorStrzelonychPC = sprawdzStrzal(zwrocCyfre(strzal_komp_x), strzal_komp_y, wektorStrzelonychPC);
				} while (!(spr - wektorStrzelonychPC.size()));
				cout << "KOMPUTER LOSUJE" << endl;
			}
			else //je�li mamy podejrzane punkty
			{
				cout << "KOMPUTER WYBIERA" << endl;
				if (zapamietane.size() > 1) //wykrywanie kierunku
				{
					if (zapamietane[0].x == zapamietane[1].x)
						poziom = 1;
					else
						pion = 1;
				}
				licznik_pom = 0;
				for (int liczbaTrafien = 0; liczbaTrafien < zapamietane.size(); liczbaTrafien++)
				{
					if (zapamietane[liczbaTrafien].y < (N - 1) && tab1[zapamietane[liczbaTrafien].x][zapamietane[liczbaTrafien].y+1] != 'X' && tab1[zapamietane[liczbaTrafien].x][zapamietane[liczbaTrafien].y+1] != '#' && pion!=1)//poruszamy sie w prawo
					{
						cout << "Strzelono w prawo" << endl;
						strzal_komp_x = zwrocLitere(zapamietane[liczbaTrafien].x);
						strzal_komp_y = zapamietane[liczbaTrafien].y + 1;
					}
					else //nie mozna w prawo, poruszamy sie inaczej
					{
						if (zapamietane[liczbaTrafien].y >0  && tab1[zapamietane[liczbaTrafien].x][zapamietane[liczbaTrafien].y-1] != 'X' && tab1[zapamietane[liczbaTrafien].x][zapamietane[liczbaTrafien].y-1] != '#' && pion != 1)//poruszamy sie w lewo
						{
							cout << "Strzelono w lewo" << endl;
							strzal_komp_x = zwrocLitere(zapamietane[liczbaTrafien].x);
							strzal_komp_y = zapamietane[liczbaTrafien].y-1;
						}
						else ////nie mozna w lewo, poruszamy sie inaczej
						{
							if (zapamietane[liczbaTrafien].x <( N - 1) && tab1[zapamietane[liczbaTrafien].x+1][zapamietane[liczbaTrafien].y] != 'X' && tab1[zapamietane[liczbaTrafien].x+1][zapamietane[liczbaTrafien].y] != '#' && poziom != 1)//poruszamy sie w d�
							{
								cout << "Strzelono w dol" << endl;
								strzal_komp_x = zwrocLitere(zapamietane[liczbaTrafien].x + 1);
								strzal_komp_y = zapamietane[liczbaTrafien].y;
							}
							else //nie mozna w dol poruszamy sie w g�r�
							{
								if (zapamietane[liczbaTrafien].x >0 && tab1[zapamietane[liczbaTrafien].x-1][zapamietane[liczbaTrafien].y] != 'X' && tab1[zapamietane[liczbaTrafien].x-1][zapamietane[liczbaTrafien].y] != '#' && poziom != 1)
								{
									cout << "Strzelono w gore" << endl;
									strzal_komp_x = zwrocLitere(zapamietane[liczbaTrafien].x - 1);
									strzal_komp_y = zapamietane[liczbaTrafien].y;
								}
								else
								{
									licznik_pom++;
									if (licznik_pom == zapamietane.size())
									{
										cout << "ZESTRZELONO "<<zapamietane.size()<<"-masztowiec" << endl;
										polozenie = 0; //szukaj poczatku statku
										if (pion == 1)
											polozenie = 1;
										otoczka_x = zwrocLitere(zapamietane[0].x);
										otoczka_y = zapamietane[0].y;
										for (int i = 1; i < zapamietane.size(); i++)//znalezienie wlasciwego pola poczatkowego
										{
											if (zapamietane[i].x < zwrocCyfre(otoczka_x))
												otoczka_x = zwrocLitere(zapamietane[i].x);
											if (zapamietane[i].y < otoczka_y)
												otoczka_y = zapamietane[i].y;
										}
										tab1 = dodajOtoczke(zwrocCyfre(otoczka_x), otoczka_y, polozenie, zapamietane.size(), tab1,1); //dodawanie otoczki zestrzelonego statku
										wyswietlStatki(tab1);
										zapamietane.clear(); //czyszczenie wektora podejrzanych punkt�w
										licznik_pom = 0; // czyszczenie zmiennych pomocniczych do znajdowania statku
										pion = poziom = 0;
										for (int i = 0; i < zapamietaneNIE.size(); i++) //uaktualnienie niepotrzebnych p�l
											wektorStrzelonychPC = sprawdzStrzal(zapamietaneNIE[i].x, zapamietaneNIE[i].y, wektorStrzelonychPC);
										zapamietaneNIE.clear(); //czyszczenie wektora niepotrzebnych punktow z ramki
										los_1 = rand() % wektorStrzelonychPC.size(); //ponowne losowanie, gdy zestrzelono statek
										strzal_komp_x = zwrocLitere(wektorStrzelonychPC[los_1].x);
										strzal_komp_y = wektorStrzelonychPC[los_1].y;
										cout << "KOMPUTER LOSUJE2" << endl;
									}
								}
							}
						}
					}
					spr = wektorStrzelonychPC.size();
					wektorStrzelonychPC = sprawdzStrzal(zwrocCyfre(strzal_komp_x), strzal_komp_y, wektorStrzelonychPC);
					if (spr - wektorStrzelonychPC.size())
						break;
				}
			}
			cout << endl << "KOMPUTER STRZELIL: " << strzal_komp_x << strzal_komp_y << endl;

			//Zaznacz punkt na mojej planszy
			if (tab1[zwrocCyfre(strzal_komp_x)][strzal_komp_y] == 'S')
			{
				tab1[zwrocCyfre(strzal_komp_x)][strzal_komp_y] = '#';
				wektorTrafionych = sprawdz(zwrocCyfre(strzal_komp_x), strzal_komp_y, wektorTrafionych);
				trafionyPC = true;
				trafionePole.x = zwrocCyfre(strzal_komp_x);
				trafionePole.y = strzal_komp_y;
				zapamietane.push_back(trafionePole);
				if (wektorTrafionych.size() == 0)
					goto WYGRANA;
			}
			else
			{
				tab1[zwrocCyfre(strzal_komp_x)][strzal_komp_y] = 'X';
				if (zapamietane.size())
				{
					trafionePole.x = zwrocCyfre(strzal_komp_x);
					trafionePole.y = strzal_komp_y;
					zapamietaneNIE.push_back(trafionePole);
				}
			}
			cout << "PLANSZA KOMPUTERA 2" << endl;
			wyswietlStatki(tab1);
		} while (trafionyPC); //r�b dop�ki trafiony
		


		/*cout << endl << "PLANSZA KOMPUTERA" << endl;
		wyswietlStatki(tabpom);
		do
		{
			trafiony = false;
			cout << endl << "STRZELAJ: " << endl;
			licznik = 0;
			do
			{
				if (licznik != 0)
					cout << "STRZELAJ POPRAWNIE" << endl;
				spr = wektorStrzelonych.size();
				cin >> strzal_x;
				cin >> strzal_y;
				wektorStrzelonych = sprawdzStrzal(zwrocCyfre(strzal_x), strzal_y, wektorStrzelonych);
				licznik++;
			} while ( !(spr - wektorStrzelonych.size()) );

			//Zaznacz punkt na planszy komputera i pomocniczej planszy
			cout << endl << "PLANSZA KOMPUTERA" << endl;
			if (tab2[zwrocCyfre(strzal_x)][strzal_y] == 'S')
			{
				tab2[zwrocCyfre(strzal_x)][strzal_y] = '#';
				tabpom[zwrocCyfre(strzal_x)][strzal_y] = '#';
				wektorTrafionychPC=sprawdz(zwrocCyfre(strzal_x), strzal_y, wektorTrafionychPC);
				trafiony = true;
				
				if (wektorTrafionychPC.size() == 0)
					goto WYGRANA;
				wyswietlStatki(tabpom);
			}
			else
			{
				tabpom[zwrocCyfre(strzal_x)][strzal_y] = 'X';
				tab2[zwrocCyfre(strzal_x)][strzal_y] = 'X';
				wyswietlStatki(tabpom);
				cout << "DODAC OTOCZKE STATKU? [Y/N]" << endl;
				cin >> opt;
			}
			if (opt == 'Y')
			{
				cout << endl << "Podaj wspolrzedne poczatku statku " << endl;
				cin >> otoczka_x;
				cin >> otoczka_y;
				cout << "Wybierz polozenie: 0 - poziomo / 1 - pionowo " << endl;
				cin >> polozenie;
				cout << "Podaj dlugosc statku: ";
				cin >> dlugosc;
				tabpom=dodajOtoczke(zwrocCyfre(otoczka_x),otoczka_y,polozenie,dlugosc,tabpom,0);
				cout << endl << "PLANSZA KOMPUTERA" << endl;
				wyswietlStatki(tabpom);
				opt = 'N';
			}	
		} while (trafiony); // r�b dop�ki trafiony*/

		do
		{
			trafiony = false;
			cout << "KOMPUTER2 WYKONUJE RUCH: " << endl;
			Sleep(2000);
			if (zapamietane2.size() == 0) //je�li nie mamy podejrzanych punkt�w
			{
				do
				{
					spr2 = wektorStrzelonych.size();
					los_2 = rand() % wektorStrzelonych.size();
					strzal_x = zwrocLitere(wektorStrzelonych[los_2].x);
					strzal_y = wektorStrzelonych[los_2].y;
					wektorStrzelonych = sprawdzStrzal(zwrocCyfre(strzal_x), strzal_y, wektorStrzelonych);
				} while (!(spr2 - wektorStrzelonych.size()));
				cout << "KOMPUTER2 LOSUJE" << endl;
			}
			else //je�li mamy podejrzane punkty
			{
				cout << "KOMPUTER2 WYBIERA" << endl;
				if (zapamietane2.size() > 1) //wykrywanie kierunku
				{
					if (zapamietane2[0].x == zapamietane2[1].x)
						poziom2 = 1;
					else
						pion2 = 1;
				}
				licznik_pom2 = 0;
				for (int liczbaTrafien2 = 0; liczbaTrafien2 < zapamietane2.size(); liczbaTrafien2++)
				{
					if (zapamietane2[liczbaTrafien2].y < (N - 1) && tab2[zapamietane2[liczbaTrafien2].x][zapamietane2[liczbaTrafien2].y + 1] != 'X' && tab2[zapamietane2[liczbaTrafien2].x][zapamietane2[liczbaTrafien2].y + 1] != '#' && pion2 != 1)//poruszamy sie w prawo
					{
						cout << "Strzelono w prawo" << endl;
						strzal_x = zwrocLitere(zapamietane2[liczbaTrafien2].x);
						strzal_y = zapamietane2[liczbaTrafien2].y + 1;
					}
					else //nie mozna w prawo, poruszamy sie inaczej
					{
						if (zapamietane2[liczbaTrafien2].y >0 && tab2[zapamietane2[liczbaTrafien2].x][zapamietane2[liczbaTrafien2].y - 1] != 'X' && tab2[zapamietane2[liczbaTrafien2].x][zapamietane2[liczbaTrafien2].y - 1] != '#' && pion2 != 1)//poruszamy sie w lewo
						{
							cout << "Strzelono w lewo" << endl;
							strzal_x = zwrocLitere(zapamietane2[liczbaTrafien2].x);
							strzal_y = zapamietane2[liczbaTrafien2].y - 1;
						}
						else ////nie mozna w lewo, poruszamy sie inaczej
						{
							if (zapamietane2[liczbaTrafien2].x <(N - 1) && tab2[zapamietane2[liczbaTrafien2].x + 1][zapamietane2[liczbaTrafien2].y] != 'X' && tab2[zapamietane2[liczbaTrafien2].x + 1][zapamietane2[liczbaTrafien2].y] != '#' && poziom2 != 1)//poruszamy sie w d�
							{
								cout << "Strzelono w dol" << endl;
								strzal_x = zwrocLitere(zapamietane2[liczbaTrafien2].x + 1);
								strzal_y = zapamietane2[liczbaTrafien2].y;
							}
							else //nie mozna w dol poruszamy sie w g�r�
							{
								if (zapamietane2[liczbaTrafien2].x >0 && tab2[zapamietane2[liczbaTrafien2].x - 1][zapamietane2[liczbaTrafien2].y] != 'X' && tab2[zapamietane2[liczbaTrafien2].x - 1][zapamietane2[liczbaTrafien2].y] != '#' && poziom2 != 1)
								{
									cout << "Strzelono w gore" << endl;
									strzal_x = zwrocLitere(zapamietane2[liczbaTrafien2].x - 1);
									strzal_y = zapamietane2[liczbaTrafien2].y;
								}
								else
								{
									licznik_pom2++;
									if (licznik_pom2 == zapamietane2.size())
									{
										cout << "ZESTRZELONO " << zapamietane2.size() << "-masztowiec" << endl;
										polozenie2 = 0; //szukaj poczatku statku
										if (pion2 == 1)
											polozenie2 = 1;
										otoczka_x2 = zwrocLitere(zapamietane2[0].x);
										otoczka_y2 = zapamietane2[0].y;
										for (int i = 1; i < zapamietane2.size(); i++)//znalezienie wlasciwego pola poczatkowego
										{
											if (zapamietane2[i].x < zwrocCyfre(otoczka_x2))
												otoczka_x2 = zwrocLitere(zapamietane2[i].x);
											if (zapamietane2[i].y < otoczka_y2)
												otoczka_y2 = zapamietane2[i].y;
										}
										tab2 = dodajOtoczke(zwrocCyfre(otoczka_x2), otoczka_y2, polozenie2, zapamietane2.size(), tab2, 1); //dodawanie otoczki zestrzelonego statku
										wyswietlStatki(tab2);
										zapamietane2.clear(); //czyszczenie wektora podejrzanych punkt�w
										licznik_pom2 = 0; // czyszczenie zmiennych pomocniczych do znajdowania statku
										pion2 = poziom2 = 0;
										for (int i = 0; i < zapamietaneNIE2.size(); i++) //uaktualnienie niepotrzebnych p�l
											wektorStrzelonych = sprawdzStrzal(zapamietaneNIE2[i].x, zapamietaneNIE2[i].y, wektorStrzelonych);
										zapamietaneNIE2.clear(); //czyszczenie wektora niepotrzebnych punktow z ramki
										los_2 = rand() % wektorStrzelonych.size(); //ponowne losowanie, gdy zestrzelono statek
										strzal_x = zwrocLitere(wektorStrzelonych[los_2].x);
										strzal_y = wektorStrzelonych[los_2].y;
										cout << "KOMPUTER2 LOSUJE2" << endl;
									}
								}
							}
						}
					}
					spr2 = wektorStrzelonych.size();
					wektorStrzelonych = sprawdzStrzal(zwrocCyfre(strzal_x), strzal_y, wektorStrzelonych);
					if (spr2 - wektorStrzelonych.size())
						break;
				}
			}
			cout << endl << "KOMPUTER2 STRZELIL: " << strzal_x << strzal_y << endl;

			//Zaznacz punkt na mojej planszy
			if (tab2[zwrocCyfre(strzal_x)][strzal_y] == 'S')
			{
				tab2[zwrocCyfre(strzal_x)][strzal_y] = '#';
				wektorTrafionychPC = sprawdz(zwrocCyfre(strzal_komp_x), strzal_komp_y, wektorTrafionychPC);
				trafiony = true;
				trafionePole.x = zwrocCyfre(strzal_x);
				trafionePole.y = strzal_y;
				zapamietane2.push_back(trafionePole);
				if (wektorTrafionychPC.size() == 0)
					goto WYGRANA;
			}
			else
			{
				tab2[zwrocCyfre(strzal_x)][strzal_y] = 'X';
				if (zapamietane2.size())
				{
					trafionePole.x = zwrocCyfre(strzal_x);
					trafionePole.y = strzal_y;
					zapamietaneNIE2.push_back(trafionePole);
				}
			}
			cout << "PLANSZA KOMPUTERA 1" << endl;
			wyswietlStatki(tab2);
		} while (trafiony); //r�b dop�ki trafiony

		cout <<endl<< "Nacisnij aby grac dalej ..." << endl;
		getch();
	} // KONIEC P�TLI Z GR�



WYGRANA:
	/*if (wektorTrafionychPC.size() == 0) //twoja wygrana
		cout << "*****!!!BRAWO, WYGRALES!!!*****" << endl << endl;
	else //wygrana komputera
		cout << "*****NIESTETY PRZEGRALES*****" << endl << endl;
		*/
	if (wektorTrafionychPC.size() == 0) //twoja wygrana
		cout << "*****!!!WYGRAL KOMPUTER 2!!!*****" << endl << endl;
	else //wygrana komputera
		cout << "*****WYGRAL KOMPUTER 1*****" << endl << endl;
	
	/*cout << endl << "MOJA PLANSZA" << endl;
	wyswietlStatki(tab1);
	cout << endl << "PLANSZA KOMPUTERA" << endl;
	wyswietlStatki(tabpom);*/
	cout << endl << "PLANSZA KOMPUTERA 2" << endl;
	wyswietlStatki(tab1);
	cout << endl << "PLANSZA KOMPUTERA 1" << endl;
	wyswietlStatki(tab2);
	return 0;
}

