// zad1a_lista3.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
//stos bazuj�cy na tablicy
#include "stdafx.h"
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
using namespace std;

template <class E>
class stos {
	int rozmiar;
	int t;
	E *tab;

public:
	stos() { t = 0; rozmiar = 1; tab = new E[rozmiar]; };

	bool Pusty();
	void Dodaj(E);
	void Dodaj2(E);
	void Usun();
	void UsunWszystko();
	void Wyswietl();
	E * Rozszerz(E*);
	E * Rozszerz2(E*);
	E * Zmniejsz(E*);
};

template <class E>
bool stos<E>::Pusty()
{
	if (t == 0  )
		return true;
	else
		return false;
}

template <class E>
void stos<E>::Dodaj(E text)
{
	if (t == rozmiar )
	{
		tab=Rozszerz(tab);
	}
	tab[t] = text; 
	t++;
}

template <class E>
void stos<E>::Dodaj2(E text)
{
	if (t == rozmiar)
	{
		tab = Rozszerz2(tab);
	}
	tab[t] = text;
	t++;
}

template <class E>
void stos<E>::Usun()
{
	if (Pusty())
		cout << "Stos jest pusty" << endl;
	else
	{
		t--;
		cout << "Usunieto: " << tab[t];
		if(rozmiar>1)
			tab=Zmniejsz(tab);
	}
}

template <class E>
void stos<E>::UsunWszystko()
{
	if (Pusty())
		cout << "Stos jest pusty" << endl;
	else
	{
		while (!Pusty())
		{
			t--;
			//cout << "Usunieto" << endl;
		}
	}
}

template <class E>
void stos<E>::Wyswietl()
{
	if (Pusty())
		cout << "Stos jest pusty" << endl;
	else
		for (int i = t-1; i >=0; i--)
			cout << tab[i] << endl;
}

template <class E>
E * stos<E>::Rozszerz(E *tab)
{
	rozmiar = rozmiar * 2;
	E *tab2 = new E[rozmiar];
	for (int i = 0; i <= rozmiar/2-1; i++)
		tab2[i] = tab[i];

	delete[]tab;
	return tab2;
}

template <class E>
E * stos<E>::Rozszerz2(E *tab)
{
	rozmiar=rozmiar+5;
	E *tab2 = new E[rozmiar];
	for (int i = 0; i <= rozmiar-5; i++)
		tab2[i] = tab[i];

	delete[]tab;
	return tab2;
}

template <class E>
E * stos<E>::Zmniejsz(E *tab)
{
	rozmiar--;
	E *tab2 = new E[rozmiar];
	for (int i = 0; i < rozmiar-1; i++)
		tab2[i] = tab[i];

	delete[]tab;
	return tab2;
}

int main()
{
	stos<string> NowyStos;
	stos<int> TestStos,TestStos2;
	int n;
	double roznica;
	string tekst;
	char opcja;
	clock_t start, stop;
	srand(time(0));
	do
	{
		cout << endl << "********* MENU - STOS(TABLICA) *********" << endl << endl;
		cout << "1. Dodaj element" << endl;
		cout << "2. Usun element" << endl;
		cout << "3. Usun wszystko" << endl;
		cout << "4. Wyswietl stos" << endl;
		cout << "5. Zakoncz program" << endl;
		cout << "6. Test x2" << endl;
		cout << "7. Test +5" << endl << endl;
		cout << "Wybierz opcje: ";

		cin >> opcja;
		switch (opcja) {
		case '1':
			cout << "Podaj tekst: " << endl;
			cin >> tekst;
			NowyStos.Dodaj(tekst);
			break;
		case '2':
			NowyStos.Usun();
			break;
		case '3':
			NowyStos.UsunWszystko();
			break;
		case '4':
			NowyStos.Wyswietl();
			break;
		case '5':
			cout << endl << "Koniec programu" << endl;
			break;
		case '6':
			cout << "Podaj ilosc danych:" << endl;
			cin >> n;
			start = clock();
			for (int i = 0; i < n; i++)
			{
				int x = rand() % 101;
				TestStos.Dodaj(x);
			}
			stop = clock();
			TestStos.UsunWszystko();
			roznica = stop - start;
			cout << "Czas: " << roznica << " [ms]" << endl;
			break;
		case '7':
			cout << "Podaj ilosc danych:" << endl;
			cin >> n;
			start = clock();
			for (int i = 0; i < n; i++)
			{
				int x = rand() % 101;
				TestStos2.Dodaj2(x);
			}
			stop = clock();
			TestStos2.UsunWszystko();
			roznica = stop - start;
			cout << "Czas: " << roznica << " [ms]" << endl;
			break;
		default:
			cout << endl << "Nieznana opcja. Wprowadz jeszcze raz" << endl << endl;
		}
	} while (opcja != '5');
	return 0;
}


