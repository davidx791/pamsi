// zad1b_lista3.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
// Stos - STL
#include "stdafx.h"
#include <iostream>
#include <string>
#include <stack>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
	stack<string> NowyStos,tmp;
	stack<int> TestStos;
	int n;
	double roznica;
	string tekst;
	char opcja;
	clock_t start, stop;
	srand(time(0));
	do
	{
		cout << endl << "********* MENU - STOS(STL) *********" << endl << endl;
		cout << "1. Dodaj element" << endl;
		cout << "2. Usun element" << endl;
		cout << "3. Usun wszystko" << endl;
		cout << "4. Wyswietl stos" << endl;
		cout << "5. Zakoncz program" << endl;
		cout << "6. Test" << endl<<endl;
		cout << "Wybierz opcje: ";

		cin >> opcja;
		switch (opcja) {
		case '1':
			cout << "Podaj tekst: " << endl;
			cin >> tekst;
			NowyStos.push(tekst);
			break;
		case '2':
			if (NowyStos.empty())
				cout << "Stos jest pusty" << endl;
			else
				NowyStos.pop();
			break;
		case '3':
			if (NowyStos.empty())
				cout << "Stos jest pusty" << endl;
			else
				while (!NowyStos.empty())
				{
					NowyStos.pop();
					cout << "Usunieto" << endl;
				}
			break;
		case '4':
			if (NowyStos.empty())
				cout << "Stos jest pusty" << endl;
			else
			{
				cout << "Zawartosc stosu: " << endl;
				tmp = NowyStos;
				while (!tmp.empty())
				{
					cout << tmp.top() << endl;
					tmp.pop();
				}
			}
			break;
		case '5':
			cout << endl << "Koniec programu" << endl;
			break;
		case '6':
			cout << "Podaj ilosc danych:" << endl;
			cin >> n;
			start = clock();
			for (int i = 0; i < n; i++)
			{
				int x = rand() % 101;
				TestStos.push(x);
			}
			stop = clock();
			while (!TestStos.empty())
				{
					TestStos.pop();
				}
			roznica = stop - start;
			cout << "Czas: " << roznica << " [ms]" << endl;
			break;
		default:
			cout << endl << "Nieznana opcja. Wprowadz jeszcze raz" << endl << endl;
		}
	} while (opcja != '5');
	return 0;
}

