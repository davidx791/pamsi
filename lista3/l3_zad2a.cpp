// zad2a_lista3.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
//Kolejka bazujaca na tablicy
#include "stdafx.h"
#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

template <typename E>
class kolejka {
	int rozmiar;
	int t;
	E *tab;

public:
	kolejka() { t = 0; rozmiar = 1; tab = new E[rozmiar]; };

	bool Pusta();
	void Dodaj(E);
	void Dodaj2(E);
	void Usun();
	void UsunWszystko();
	void Wyswietl();
	E * Rozszerz(E*);
	E * Rozszerz2(E*);
	E * Zmniejsz(E*);
};

template <typename E>
bool kolejka<E>::Pusta()
{
	if (t == 0)
		return true;
	else
		return false;
}

template <typename E>
void kolejka<E>::Dodaj(E text)
{
	if (t == rozmiar)
	{
		tab = Rozszerz(tab);
	}
	tab[t] = text;
	t++;
}

template <typename E>
void kolejka<E>::Dodaj2(E text)
{
	if (t == rozmiar)
	{
		tab = Rozszerz2(tab);
	}
	tab[t] = text;
	t++;
}

template <typename E>
void kolejka<E>::Usun()
{
	if (Pusta())
		cout << "Kolejka jest pusta" << endl;
	else
	{
		E *tmp = new E[rozmiar];
		cout << "Usunieto: " << tab[0];
		if (rozmiar > 1)
		{
			for (int i = 0; i < rozmiar; i++)
				tmp[i] = tab[i];

			for (int i = 0; i < rozmiar - 1; i++)
				tab[i] = tmp[i + 1];

			delete[]tmp;
		}
		else
		{
			tab[0] = tmp[0];
		}
		t--;
		if (rozmiar>1)
			tab = Zmniejsz(tab);
	}
}

template <typename E>
void kolejka<E>::UsunWszystko()
{
	if (Pusta())
		cout << "Kolejka jest pusta" << endl;
	else
	{
		while (!Pusta())
		{
			t--;
		
		}
	}
}

template <typename E>
void kolejka<E>::Wyswietl()
{
	if (Pusta())
		cout << "Kolejka jest pusta" << endl;
	else
		for (int i=0; i<t; i++)
			cout << tab[i] << " ";
}

template <typename E>
E * kolejka<E>::Rozszerz(E *tab)
{
	rozmiar = rozmiar * 2;
	E *tab2 = new E[rozmiar];
	for (int i = 0; i <= rozmiar / 2 - 1; i++)
		tab2[i] = tab[i];

	delete[]tab;
	return tab2;
}

template <typename E>
E * kolejka<E>::Rozszerz2(E *tab)
{
	rozmiar=rozmiar+5;
	E *tab2 = new E[rozmiar];
	for (int i = 0; i <= rozmiar-5; i++)
		tab2[i] = tab[i];

	delete[]tab;
	return tab2;
}

template <typename E>
E * kolejka<E>::Zmniejsz(E *tab)
{
	rozmiar--;
	E *tab2 = new E[rozmiar];
	for (int i = 0; i <= rozmiar - 1; i++)
		tab2[i] = tab[i];

	delete[]tab;
	return tab2;
}

int main()
{
	kolejka<string> NowaKolejka;
	kolejka<int> TestKolejka,TestKolejka2;
	int n;
	double roznica;
	string tekst;
	char opcja;
	clock_t start, stop;
	srand(time(0));
	do
	{
		cout << endl << "********* MENU - KOLEJKA(TABLICA) *********" << endl << endl;
		cout << "1. Dodaj element" << endl;
		cout << "2. Usun element" << endl;
		cout << "3. Usun wszystko" << endl;
		cout << "4. Wyswietl kolejke" << endl;
		cout << "5. Zakoncz program" << endl;
		cout << "6. Test x2" << endl;
		cout << "7. Test +5" << endl;
		cout << "Wybierz opcje: ";

		cin >> opcja;
		switch (opcja) {
		case '1':
			cout << "Podaj tekst: " << endl;
			cin >> tekst;
			NowaKolejka.Dodaj(tekst);
			break;
		case '2':
			NowaKolejka.Usun();
			break;
		case '3':
			NowaKolejka.UsunWszystko();
			break;
		case '4':
			NowaKolejka.Wyswietl();
			break;
		case '5':
			cout << endl << "Koniec programu" <<endl;
			break;
		case '6':
			cout << "Podaj ilosc danych:" << endl;
			cin >> n;
			start = clock();
			for (int i = 0; i < n; i++)
			{
				int x = rand() % 101;
				TestKolejka.Dodaj(x);
			}
			stop = clock();
			TestKolejka.UsunWszystko();
			roznica = stop - start;
			cout << "Czas: " << roznica << " [ms]" << endl;
			break;
		case '7':
			cout << "Podaj ilosc danych:" << endl;
			cin >> n;
			start = clock();
			for (int i = 0; i < n; i++)
			{
				int x = rand() % 101;
				TestKolejka2.Dodaj2(x);
			}
			stop = clock();
			TestKolejka2.UsunWszystko();
			roznica = stop - start;
			cout << "Czas: " << roznica << " [ms]" << endl;
			break;
		default:
			cout << endl << "Nieznana opcja. Wprowadz jeszcze raz" << endl << endl;
		}
	} while (opcja != '5');
	return 0;
}

