// zad2b_lista3.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
//Kolejka - STL
#include "stdafx.h"
#include <iostream>
#include <string>
#include <queue>
#include <ctime>
#include <cstdlib>

using namespace std;

int main()
{
	queue<string> NowaKolejka,tmp;
	string tekst;
	queue<int> TestKolejka;
	int n;
	double roznica;
	char opcja;
	clock_t start, stop;
	srand(time(0));
	do
	{
		cout << endl << "********* MENU - KOLEJKA(STL) *********" << endl << endl;
		cout << "1. Dodaj element" << endl;
		cout << "2. Usun element" << endl;
		cout << "3. Usun wszystko" << endl;
		cout << "4. Wyswietl kolejke" << endl;
		cout << "5. Zakoncz program" << endl;
		cout << "6. Test" << endl<<endl;
		cout << "Wybierz opcje: ";

		cin >> opcja;
		switch (opcja) {
		case '1':
			cout << "Podaj tekst: " << endl;
			cin >> tekst;
			NowaKolejka.push(tekst);
			break;
		case '2':
			if (NowaKolejka.empty())
				cout << "Kolejka jest pusta" << endl;
			else
				NowaKolejka.pop();
			break;
		case '3':
			if (NowaKolejka.empty())
				cout << "Kolejka jest pusta" << endl;
			else
				while (!NowaKolejka.empty())
				{
					NowaKolejka.pop();
					cout << "Usunieto" << endl;
				}
			break;
		case '4':
			if (NowaKolejka.empty())
				cout << "Kolejka jest pusta" << endl;
			else
			{
				cout << "Zawartosc kolejki: " << endl;
				tmp = NowaKolejka;
				while (!tmp.empty())
				{
					cout << tmp.front() <<" ";
					tmp.pop();
				}
			}
			break;
		case '5':
			cout << endl << "Koniec programu" << endl;
			break;
		case '6':
			cout << "Podaj ilosc danych:" << endl;
			cin >> n;
			start = clock();
			for (int i = 0; i < n; i++)
			{
				int x = rand() % 101;
				TestKolejka.push(x);
			}
			stop = clock();
			while (!TestKolejka.empty())
			{
				TestKolejka.pop();
			}
			roznica = stop - start;
			cout << "Czas: " << roznica << " [ms]" << endl;
			break;
		default:
			cout << endl << "Nieznana opcja. Wprowadz jeszcze raz" << endl << endl;
		}
	} while (opcja != '5');
	return 0;
}

