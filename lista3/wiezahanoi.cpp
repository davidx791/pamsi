// wiezahanoi_lista3.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
//wieza hanoi na 3 stosach
#include "stdafx.h"
#include <iostream>
#include <string>
#include <stack>
#include <conio.h>

using namespace std;
void wyswietl();
void hanoi(int, stack<string>&, stack<string>&, stack<string>&);
stack<string> Stos1, Stos2, Stos3;
int licz=-1;
int krok;

int main()
{
	string tekst;
	int n;
	
	//Deklaracja wie�y
	cout << "Podaj krok do zatrzymania:" << endl;
	cin >> krok;
	cout << "Z ilu elementow ma sie skladac pierwsza wieza?" << endl;
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		string a;
		cout << "Podaj " << i+1 << " element wiezy: "<<endl;
		cin >> a;
		Stos1.push(a);
	}
	cout << endl << endl;
		
	wyswietl();
	hanoi(n, Stos1, Stos2, Stos3);
	cout << "licznik: " <<licz<< endl;
	
	return 0;
}

//Funkcja hanoi
void hanoi(int n,stack<string>&s1, stack<string>&s2, stack<string>&s3)
{
	string x;
	if (n > 0)
	{
		hanoi(n - 1, s1, s3, s2);
		x = s1.top();
		s1.pop();
		s3.push(x);
		wyswietl();
		hanoi(n - 1, s2, s1, s3);
	}
}

//Wyswietlanie wiez
void wyswietl()
{
	licz++;
	stack<string> tmp;
	cout << "*****Twoje wieze*****:" << endl;
	cout << "Wieza nr 1:" << endl;
	if (!Stos1.empty())
	{
		tmp = Stos1;
		while (!tmp.empty())
		{
			cout << tmp.top() << endl;
			tmp.pop();
		}
	}
	cout << "--------" << endl << endl;
	cout << "Wieza nr 2: " << endl;
	if (!Stos2.empty())
	{
		tmp = Stos2;
		while (!tmp.empty())
		{
			cout << tmp.top() << endl;
			tmp.pop();
		}
	}
	cout << "--------" << endl << endl;
	cout << "Wieza nr 3" << endl;
	if (!Stos3.empty())
	{
		tmp = Stos3;
		while (!tmp.empty())
		{
			cout << tmp.top() << endl;
			tmp.pop();
		}
	}
	cout << "--------" << endl << endl;
	if (licz == krok)
		_getch();
}
