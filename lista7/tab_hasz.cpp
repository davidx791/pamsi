// tab_hasz.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
//tablica haszujaca
#include "stdafx.h"
#include<iostream>
#include<ctime>
#include<vector>

using namespace std;

int n,m;
int h(int k);
int d(int k, int q);
bool jestPierwsza(int a);
int bylaWylosowana(int liczba,int i, int *tab);
void wyswietl(int *tab);
void wyswietl2(vector<vector<int> > tab2);

void probkowanie_liniowe(int *tab, int *tab_los,int m);
void dodajprob(int *tab, int a);
void usunprob(int *tab, int a);

void podwojne_haszowanie(int *tab, int *tab_los, int m,int q);
void dodajpodwojne(int *tab, int a,int q);
void usunpodwojne(int *tab, int a,int q);

vector<vector<int> > linkowanie(vector<vector<int> >tab2, int *tab_los, int m);
vector<vector<int> > dodajlink(vector<vector<int> >tab2,int a);
void usunlink(vector<vector<int> >tab2, int a);
int main()
{
	int a,b;
	srand(time(0));
	cout << "Podaj rozmiar tablicy (liczba pierwsza)" << endl;
	cin >> n;
	int *tab = new int[n],*tab_los = new int[n];
	vector<vector<int> > tab2;
	vector<int> v(1);
	if (!jestPierwsza(n)) //sprawdzenie czy pierwsza
		cout << "To nie jest liczba pierwsza" << endl;
	else
	{
		//Losowanie
		cout << "Ile wylosowac liczb?" << endl;
		cin >> m;
		tab_los[0] = rand() % (2 * n) + 1;
		for (int i = 1; i < m; i++)
		{
			int tmp = rand() % (2 * n) + 1;
			while (bylaWylosowana(tmp, i, tab_los)) //sprawdzanie czy juz by�a
				tmp = rand() % (2 * n) + 1; //i wylosowanie nowej liczby
			tab_los[i] = tmp;
		}
		for (int i = m; i < n; i++)
			tab_los[i] = 0;

		//	int tab_los[7] = { 76,93,40,47,10,55 };

		cout << "Wylosowano: " << endl;
		for (int i = 0; i < n; i++)
		{
			if (tab_los[i] != 0)
				cout << tab_los[i] << " ";
			tab[i] = NULL;//utworzenie pustej tablicy
		}
		
		//utworzenie vectora dwuwymiarowego
		for (int i = 0; i < n; i++)
			tab2.push_back(v);
		
		//pusty vector
		cout << endl;
		for (int i = 0; i < n; i++)
			for (int j = 0; j < v.size(); j++)
				tab2[i][j] = 0;
		
/*******************************************************************************/
//Probkowanie liniowe
		//probkowanie_liniowe(tab, tab_los,m);
		
//Podwojne haszowanie
		cout << endl << "Podaj wartosc q dla drugiej funkcji haszujacej," << endl;
		cout << "q musi byc liczba pierwsza i mniejsza od N" << endl;
		int q;
		while (true)
		{
			cin >> q;
			if (jestPierwsza(q) && q < n)
				break;
			else
				cout << "Podaj poprawna liczbe" << endl;
		}
		podwojne_haszowanie(tab, tab_los, m, q);

//Linkowanie
		//tab2=linkowanie(tab2,tab_los,m);

/********///Dodawanie
		cout <<endl<< "Podaj wartosc do dodania: " << endl;
		cin >> a;
		//dodajprob(tab, a);
		dodajpodwojne(tab, a, q);
		//tab2=dodajlink(tab2,a);

/********///Usuwanie
		cout <<endl<< "Podaj wartosc do usuniecia: " << endl;
		cin >> b;
				
		//usunprob(tab, b);
		usunpodwojne(tab, b, q);
		//usunlink(tab2, b);
		
/*******************************************************************************/
		
	}
	cout << endl;
	return 0;
}

int h(int k) { return k%n; }
int d(int k, int q) { return q - k%q; }

bool jestPierwsza(int a)
{
	if(a<2)
		return false; //nie jest pierwsza
	for(int i=2;i*i<=a;i++)
		if(a%i==0)
			return false; //znajdujemy dzielnik wiec nie jest pierwsza
	return true;//jest pierwsza
} 

int bylaWylosowana(int liczba,int i, int *tab)
{
	int j;
	for (j = 0; j <i;j++ ) // przyrownujemy z poprzednimi elementami tablicy
		if (tab[j] == liczba)
			return true;
	return false;
}

void probkowanie_liniowe(int *tab, int *tab_los, int m)
{
	int licz = 0;
	for (int i = 0; i < m; i++)
	{
		int tmp = h(tab_los[i]);
		while (!(tab[tmp] == 0))
		{
			tmp++;
			licz++;
			if (tmp == n)
				tmp = 0;
		}
		tab[tmp] = tab_los[i];
		licz++;
	}
	cout << "Licznik: " << licz << endl;
	wyswietl(tab);
}

void podwojne_haszowanie(int *tab, int *tab_los, int m, int q)
{
	int licz = 0;
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j <m; j++)
		{
			licz++;
			int tmp = (h(tab_los[i]) + j*d(tab_los[i], q)) % n;
			if (tab[tmp] == 0)
			{
				tab[tmp] = tab_los[i];
				break;
			}
		}
	}
	cout << "Licznik: " << licz << endl;
	wyswietl(tab);
}

vector<vector<int> > linkowanie(vector<vector<int> >tab2, int *tab_los, int m)
{
	int licz = 0;
	for (int i = 0; i < m; i++)
	{
		int tmp = h(tab_los[i]);
		if (tab2[tmp][0] == 0)
			tab2[tmp][0] = tab_los[i];
		else
			tab2[tmp].push_back(tab_los[i]);
		licz = licz + tab2[tmp].size();
	}
	cout << "Licznik: " << licz << endl;
	wyswietl2(tab2);
	return tab2;
}

void dodajprob(int *tab,int a)
{
	int licz = 0;
	int tmp = h(a);
	while (!(tab[tmp] == 0))
	{
		tmp++;
		licz++;
		if (tmp == n)
			tmp = 0;
	}
	tab[tmp] = a;
	licz++;
	
	cout << "Licznik dodawania: " << licz << endl;
	wyswietl(tab);
}

void usunprob(int *tab, int a)
{
	int licz = 1;
	int tmp = a%n;
	while (true)
	{
		if ((tab[tmp] == a))
		{
			tab[tmp]=0;
			break;
		}
		tmp++;
		licz++;
		if (a%n - 1 == tmp)
		{
			cout << "Nie ma takiej liczby" << endl;
			break;
		}
		if (tmp == n)
			tmp = 0;
	}
	cout << "Licznik wyszukiwania: " << licz << endl;
	wyswietl(tab);
}

void dodajpodwojne(int *tab, int a, int q)
{
	int licz = 0;
	for (int j = 0; j <m; j++)
	{
		licz++;
		int tmp = (h(a) + j*d(a, q)) % n;
		if (tab[tmp] == 0)
		{
			tab[tmp] = a;
			break;
		}
	}
	cout << "Licznik dodawania: " << licz << endl;
	wyswietl(tab);
}

void usunpodwojne(int *tab, int a,int q)
{
	int licz = 0;
	for (int j = 0; j <m; j++)
	{
		licz++;
		int tmp = (h(a) + j*d(a, q)) % n;
		if ((tab[tmp] == a))
		{
			tab[tmp] = 0;
			break;
		}
		if (j==m-1)
			cout << "Nie ma takiej liczby" << endl;
	}
	cout << "Licznik wyszukiwania: " << licz << endl;
	wyswietl(tab);
}

vector<vector<int> > dodajlink(vector<vector<int> >tab2, int a)
{
	int licz = 0;
	int tmp = h(a);
	if (tab2[tmp][0] == 0)
		tab2[tmp][0] = a;
	else
		tab2[tmp].push_back(a);
	licz = licz + tab2[tmp].size();
	
	cout << "Licznik: " << licz << endl;
	wyswietl2(tab2);
	return tab2;
}

void usunlink(vector<vector<int> >tab2, int a)
{
	int licz = 0;
	int tmp = h(a);
	if (tab2[tmp][0] == 0)
		cout << "Brak takiej wartosci" << endl;
	else
	{
		if (tab2[tmp].size() == 1 && tab2[tmp][0] == a)
		{
			tab2[tmp][0] = 0;
			licz++;
		}
		else
		{
			for (int j = 0; j < tab2[tmp].size(); j++)
			{
				licz++;
				if (tab2[tmp][j] == a)
				{
					int i = 0;
					tab2[tmp][j] = 0;
					int *tabpom=new int[tab2[tmp].size() - 1];
					for (int j = 0; j < tab2[tmp].size(); j++)
						if (tab2[tmp][j] != 0)
						{
							tabpom[i] = tab2[tmp][j];
							i++;
						}
					tab2[tmp].pop_back();
					for (int j = 0; j < tab2[tmp].size(); j++)
						tab2[tmp][j] = tabpom[j];			
				}
			}
		}
	}
		
	cout << "Licznik: " << licz << endl;
	wyswietl2(tab2);
}

void wyswietl(int *tab)
{
	for (int i = 0; i < n; i++)
	{
		if (tab[i] == 0)
			cout << " __ ";
		else
			cout << tab[i] << " ";
	}
}

void wyswietl2(vector<vector<int> > tab2)
{
	for (int i = 0; i < n; i++)
	{
		if (tab2[i][0] == 0)
			cout << "__ " << endl;
		else
		{
			for (int j = 0; j < tab2[i].size(); j++)
			{
				if (tab2[i][j] != 0)
					cout << tab2[i][j] << " ";
			}
			cout << endl;
		}
	}
}