// drzewo_avl.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//

//drzewo binarne
#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;
string cr, cl, cp; //do wyswietlania drzewa

class dane {
	int wart,roznica,roznica2;
	dane * rodzic;
	dane * lewy;
	dane * prawy;
public:
	dane() { zmienPrawy(NULL); zmienLewy(NULL); zmienRodzic(NULL); zmienRoznica(0); }
	int wezWart() { return wart; }
	int wezRoznica() { return roznica; }
	int wezRoznica2() { return roznica2; }
	dane *wezPrawy() { return prawy; }
	dane *wezLewy() { return lewy; }
	dane *wezRodzic() { return rodzic; }
	
	void zmienWart(int n_wart) { wart = n_wart; }
	void zmienPrawy(dane* n_prawy) { prawy = n_prawy; }
	void zmienLewy(dane* n_lewy) { lewy = n_lewy; }
	void zmienRodzic(dane* n_rodzic) { rodzic = n_rodzic; }
	void zmienRoznica(int n_roznica) { roznica = n_roznica; }
	void zmienRoznica2(int n_roznica2) { roznica2 = n_roznica2; }
	bool jestKorzen() { return rodzic == NULL; }
	bool jestZewnetrzny() { if (lewy == NULL && prawy == NULL) return true; else return false; }
};

/*class drzewo_binarne {
	dane * root;
public:
	drzewo_binarne() { root = NULL; }
	dane* wezRoot() { return root; }
	//void zmienRoot(dane* n_root) { root = n_root; }
	bool Puste();
	void DodajElement(int, dane*);
	void Usun(int, dane*);
	void Wyswietl_Pre_Order(dane *);
	void Wyswietl_Post_Order(dane *);
	void Wyswietl_In_Order(dane *);
	int Wysokosc(dane*);
	int max(int a, int b);
	int info(drzewo_binarne D1, dane*tmp,int licz);
	void dodanieRoznic(drzewo_binarne D1, dane*tmp);
	void rotacjaRR(dane* root, dane* tmp);
	void rotacjaLL(dane* root, dane* tmp);
	void rotacjaRL(dane* root, dane* tmp);
	void rotacjaLR(dane* root, dane* tmp);
};*/

void rysuj(string sp, string sn, dane* tmp);
void rotacjaRR(dane*& root, dane* tmp);
void rotacjaLL(dane*& root, dane* tmp);
void rotacjaRL(dane*& root, dane* tmp);
void rotacjaLR(dane*& root, dane* tmp);
void dodajElement(int x, dane *&root);
dane* usunElement(dane *&root, dane *tmp);
int wysokosc(dane*tmp);
int max(int a, int b);
void dodanieRoznic(dane*tmp);
dane *wezelZastepujacy(dane * tmp);
dane * wyszukiwanie(int x, dane *tmp);

//Wysokosc drzewa
int wysokosc(dane*tmp)
{
	if (tmp == NULL)
		return -1;
	else
		return max(wysokosc(tmp->wezLewy()), wysokosc(tmp->wezPrawy())) + 1;
}

//Zwraca maks
int max(int a, int b)
{
	if (a >= b)
		return a;
	else
		return b;
}

//Rysowanie drzewa
void rysuj(string sp, string sn, dane*tmp)
{
	string s;
	if (tmp)
	{	
		s = sp;
		if (sn == cr) s[s.length() - 2] = ' ';
			rysuj(s + cp, cr, tmp->wezPrawy());

		s = s.substr(0, sp.length() - 2);
		cout << s << sn << tmp->wezWart() << "(" << tmp->wezRoznica2() << ")" << endl;

		s = sp;
		if (sn == cl) s[s.length() - 2] = ' ';
			rysuj(s + cp, cl, tmp->wezLewy());
	}
}

//Roznice w wysokosci poddrzew
void dodanieRoznic(dane*tmp)
{
	dane*lewe = tmp->wezLewy();
	if (lewe)
		dodanieRoznic(lewe);

	dane*prawe = tmp->wezPrawy();
	if (prawe)
		dodanieRoznic(prawe);

	int hl, hp;
	if (tmp->wezLewy() != NULL)
		hl = wysokosc(tmp->wezLewy());
	else
		hl = -1;
	if (tmp->wezPrawy() != NULL)
		hp = wysokosc(tmp->wezPrawy());
	else
		hp = -1;
	tmp->zmienRoznica2(hl - hp);
}

//Funkcja znajduje w�z�a ktorym moze zastapic usuwany
dane *wezelZastepujacy(dane * tmp)
{
	dane *tmp2;
	if (tmp)
	{
		if (tmp->wezLewy()) //bierzemy pierwszy wezel z lewej
		{
			tmp = tmp->wezLewy(); //ustawiamy jako tmp
			while (tmp->wezPrawy()) 
				tmp = tmp->wezPrawy(); //schodzimy do maksymalnie prawego ktory moze go zastapic
		}
		else // jesli nie istnieje 
		{
			do
			{
				tmp2 = tmp;
				tmp = tmp->wezRodzic();
			} while (tmp && tmp->wezPrawy() != tmp2);
		}
	}
	
	return tmp;
}

//Szukanie wez�a
dane * wyszukiwanie(int x,dane *tmp)
{
	while (tmp && tmp->wezWart() != x) //szukamy wezla o danej wartosci
	{
		if (x < tmp->wezWart())
			tmp=tmp->wezLewy();
		else
			tmp=tmp->wezPrawy();
	}
	return tmp; 
}

//Rotacja RR
void rotacjaRR(dane*&root, dane* tmp)
{
	cout << "Rotacja RR" << endl;
	dane* A = tmp, *B = A->wezPrawy(), *pom = A->wezRodzic();
	
	A->zmienPrawy(B->wezLewy());
	if (A->wezPrawy())
		A->wezPrawy()->zmienRodzic(A);

	B->zmienLewy(A);
	B->zmienRodzic(pom);
	A->zmienRodzic(B);

	if (pom)
	{
		if (pom->wezLewy() == A)
			pom->zmienLewy(B);
		else
			pom->zmienPrawy(B);
	}
	else
		root = B;
	
	//zmiana roznic po rotacji
	if (B->wezRoznica() == -1)
	{
		A->zmienRoznica(0);
		B->zmienRoznica(0);
	}
	else
	{
		A->zmienRoznica(-1);
		B->zmienRoznica(1);
	}
}

//Rotacja LL
void rotacjaLL(dane*& root, dane* tmp)
{
	cout << "Rotacja LL" << endl;
	dane* A = tmp, *B = tmp->wezLewy(), *pom = tmp->wezRodzic();

	A->zmienLewy(B->wezPrawy());
	if (A->wezLewy())
		A->wezLewy()->zmienRodzic(A);
	B->zmienPrawy(A);
	B->zmienRodzic(pom);
	A->zmienRodzic(B);
	if (pom)
	{
		if (pom->wezLewy() == A)
			pom->zmienLewy(B);
		else
			pom->zmienPrawy(B);
	}
	else
		root = B;
	
	//zmiana roznic po rotacji
	if (B->wezRoznica() == 1)
	{
		A->zmienRoznica(0);
		B->zmienRoznica(0);
	}
	else
	{
		A->zmienRoznica(1);
		B->zmienRoznica(-1);
	}
}

//Rotacja RL
void rotacjaRL(dane* &root, dane* tmp)
{
	cout << "Rotacja RL" << endl;
	dane* A = tmp, *B = A->wezPrawy(), *C = B->wezLewy(), *pom = A->wezRodzic();

	B->zmienLewy(C->wezPrawy());
	if (B->wezLewy())
		B->wezLewy()->zmienRodzic(B);

	A->zmienPrawy(C->wezLewy());
	if (A->wezPrawy())
		A->wezPrawy()->zmienRodzic(A);

	C->zmienLewy(A);
	C->zmienPrawy(B);
	A->zmienRodzic(C);
	B->zmienRodzic(C);
	C->zmienRodzic(pom);
	if (pom)
	{
		if (pom->wezLewy() == A)
			pom->zmienLewy(C);
		else
			pom->zmienPrawy(C);
	}
	else
		root = C;
	
	//zmiana roznic po rotacji
	A->zmienRoznica(0);
	B->zmienRoznica(0);
	if (C->wezRoznica() == -1)
		A->zmienRoznica(1);
	if (C->wezRoznica() == 1)
		B->zmienRoznica(-1);
	C->zmienRoznica(0);

}

//Rotacja LR
void rotacjaLR(dane* &root, dane* tmp)
{
	cout << "Rotacja LR" << endl;
	dane* A = tmp, *B = A->wezLewy(), *C = B->wezPrawy(), *pom = A->wezRodzic();

	B->zmienPrawy(C->wezLewy());
	if (B->wezPrawy())
		B->wezPrawy()->zmienRodzic(B);

	A->zmienLewy(C->wezPrawy());
	if (A->wezLewy())
		A->wezLewy()->zmienRodzic(A);

	C->zmienPrawy(A);
	C->zmienLewy(B);
	A->zmienRodzic(C);
	B->zmienRodzic(C);
	C->zmienRodzic(pom);
	if (pom)
	{
		if (pom->wezLewy() == A)
			pom->zmienLewy(C);
		else
			pom->zmienPrawy(C);
	}
	else
		root = C;
	
	//zmiana roznic po rotacji
	A->zmienRoznica(0);
	B->zmienRoznica(0);
	if (C->wezRoznica() == -1)
		B->zmienRoznica(1);
	if (C->wezRoznica() == 1)
		A->zmienRoznica(-1);
	C->zmienRoznica(0);

}

//Dodawanie elementu do drzewa
void dodajElement(int x, dane *&root)
{
	dane *nowy = new dane, *tmp,*tmp2;
	bool a;

	//w konstruktorze tworzymy, tutaj tylko wartosc
	nowy->zmienWart(x);

//Znajdujemy miejsce dla nowego wez�a
	tmp = root;
	if (!tmp)
	{
		root = nowy; // je�li drzewo puste, w�ze� jest rootem
		cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
		dodanieRoznic(root);
		rysuj("", "", root);
	}
	else// jak nie to porownujemy w celu znaleznienia miejsca
	{
		while (true)
		{
			if (x < tmp->wezWart())
			{
				if (!(tmp->wezLewy())) // gdy nie ma lewego syna
				{
					tmp->zmienLewy(nowy);//to go przypisujemy
					break;
				}
				tmp = tmp->wezLewy(); //przechodzimy do lewego syna
			}
			else
			{
				if (!(tmp->wezPrawy())) // gdy nie ma prawego syna
				{
					tmp->zmienPrawy(nowy);//to go przypisujemy
					break;
				}
				tmp = tmp->wezPrawy(); //przechodzimy do prawego syna
			}
		}
		nowy->zmienRodzic(tmp);//przypisujemy ojca
		cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
		dodanieRoznic(root);
		rysuj("", "", root);
		

		//Rownowa�ymy drzewo
		if (tmp->wezRoznica())
			tmp->zmienRoznica(0);
		else
		{
			if (tmp->wezLewy() == nowy)
				tmp->zmienRoznica(1);
			else
				tmp->zmienRoznica(-1);

			tmp2 = tmp->wezRodzic();
			a = false;
			while (tmp2)
			{
				if (tmp2->wezRoznica())
				{
					a = true;
					break;
				};
				if (tmp2->wezLewy() == tmp)
					tmp2->zmienRoznica(1);
				else
					tmp2->zmienRoznica(-1);

				tmp = tmp2;
				tmp2 = tmp2->wezRodzic();
			}
			
			if (a) //musimy zr�wnowa�y� drzewo w zale�nos�i od wsp�czynnik�w
			{
				if (tmp2->wezRoznica() == 1)
				{
					if (tmp2->wezPrawy() == tmp)
						tmp2->zmienRoznica(0);
					else
					{
						if (tmp->wezRoznica() == -1)
							rotacjaLR(root, tmp2);
						else
							rotacjaLL(root, tmp2);
						cout << "Po zrownowazeniu" << endl;
						cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
						dodanieRoznic(root);
						rysuj("", "", root);
					}
				}
				else //if(tmp2->wezRoznica() == -1)
				{
					if (tmp2->wezLewy() == tmp)
						tmp2->zmienRoznica(0);
					else
					{
						if (tmp->wezRoznica() == 1)
							rotacjaRL(root, tmp2);
						else//if(tmp->wezRoznica() == -1)
							rotacjaRR(root, tmp2);
						cout << "Po zrownowazeniu" << endl;
						cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
						dodanieRoznic(root);
						rysuj("", "", root);
					}
				}
				
			}
		}

	}
}

//Usuwanie elementu
dane * usunElement(dane *&root, dane *tmp)
{
	dane  *tmp2,*tmp3,*t;
	bool a;

	if (tmp->wezLewy() && tmp->wezPrawy())//jest dwoch synow
	{
		tmp2 = usunElement(root, wezelZastepujacy(tmp));
		a = false;
	
	}
	else
	{
		if (tmp->wezLewy()) //jest tylko lewy syn
		{
			tmp2 = tmp->wezLewy();
			tmp->zmienLewy(NULL);
		}
		else
		{
			tmp2 = tmp->wezPrawy();
			tmp->zmienPrawy(NULL);
		}
		tmp->zmienRoznica(0);
		a = true;
		
	}

	if (tmp2) //gdy istnieje tmp2
	{
		tmp2->zmienRodzic(tmp->wezRodzic());
		tmp2->zmienLewy(tmp->wezLewy());
		if (tmp2->wezLewy())//gdy przypisano do lewego syna nie NULL
			tmp2->wezLewy()->zmienRodzic(tmp2);
		tmp2->zmienPrawy(tmp->wezPrawy());
		if (tmp2->wezPrawy())//gdy przypisano do prawego syna nie NULL
			tmp2->wezPrawy()->zmienRodzic(tmp2);
		tmp2->zmienRoznica(tmp->wezRoznica());

	}

	if (tmp->wezRodzic()) // jesli istnieje rodzic
	{
		if (tmp->wezRodzic()->wezLewy()==tmp)
			tmp->wezRodzic()->zmienLewy(tmp2);
		else 
			tmp->wezRodzic()->zmienPrawy(tmp2);
	}
	else root = tmp2; //gdy nie istnieje to jest rootem

	/*cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
	dodanieRoznic(root);
	rysuj("", "", root);*/
	
	//rotacje
	if (a)
	{	
		tmp3 = tmp2;
		tmp2 = tmp->wezRodzic();
		while (tmp2)
		{
			if (!(tmp2->wezRoznica()))// 1. przypadek
			{              
				if (tmp2->wezLewy() == tmp3)
					tmp2->zmienRoznica(-1);
				else 
					tmp2->zmienRoznica(1);
				break;
			}
			else // 2. i 3. przypadek
			{
				if (((tmp2->wezRoznica() == 1) && (tmp2->wezLewy() == tmp3)) || ((tmp2->wezRoznica() == -1) && (tmp2->wezPrawy() == tmp3))) //2. przypadek
				{           
					tmp2->zmienRoznica(0);
					tmp3 = tmp2;
					tmp2 = tmp2->wezRodzic();
				}
				else //3. przypadek
				{
					if (tmp2->wezLewy()== tmp3)  
						t = tmp2->wezPrawy();
					else
						t = tmp2->wezLewy();
					if (!(t->wezRoznica()))//3.1. przypadek
					{         
						if (tmp2->wezRoznica() == 1) 
							rotacjaLL(root, tmp2);
						else 
							rotacjaRR(root, tmp2);
						cout << "Po zrownowazeniu" << endl;
						/*cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
						dodanieRoznic(root);
						rysuj("", "", root);*/
						break;
					}
					else//3.2. i 3.3. przypadek
					{
						if (tmp2->wezRoznica() == t->wezRoznica())//3.2. przypadek
						{         
							if (tmp2->wezRoznica() == 1) 
								rotacjaLL(root, tmp2); 
							else 
								rotacjaRR(root, tmp2);
							tmp3 = t;
							tmp2 = t->wezRodzic();
							cout << "Po zrownowazeniu" << endl;
							/*cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
							dodanieRoznic(root);
							rysuj("", "", root);*/
						}
						else//3.3. przypadek
						{         
							if (tmp2->wezRoznica() == 1) 
								rotacjaLR(root, tmp2);
							else 
								rotacjaRL(root, tmp2);
							tmp3 = tmp2->wezRodzic(); 
							tmp2 = tmp3->wezRodzic();
							cout << "Po zrownowazeniu" << endl;
							/*cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
							dodanieRoznic(root);
							rysuj("", "", root);*/
						}
					}
				}
			}
		}
	}
	return tmp;
}

int main()
{
	dane* root = NULL;
	int x;
	//int licz = 0;
	//drzewo_binarne D1;
	cr = cl = cp = "  ";
	cr[0] = 218; cr[1] = 196;
	cl[0] = 192; cl[1] = 196;
	cp[0] = 179;

	/*cout << "Wprowadzenie tablicy elementow: 50 100 10 30 20 25 80 60 70 90" << endl;
	int tab[10] = { 50,100,10,30,20,25,80,60,70,90 };*/
	for (int i = 1; i <= 8; i++)
	{
		cout << "Dodaj element do drzewa: " << endl;
		cin >> x;
		//x = tab[i - 1];
		dodajElement(x, root);
		cout << endl;
	}
	//D1.dodanieRoznic(D1,D1.wezRoot());
	//rysuj("", "", root);
	//licz=D1.info(D1, D1.wezRoot(),licz);
	for (int i = 1; i <= 4; i++)
	{
		cout << "Usun element drzewa: " << endl;
		cin >> x;
		dane* znaleziony = wyszukiwanie(x, root);
		dane* usuniety = usunElement(root, znaleziony);
		cout << "Usunieto: " << usuniety->wezWart();
		cout << endl;
		cout << "Wysokosc drzewa: " << wysokosc(root) << endl;
		dodanieRoznic(root);
		rysuj("", "", root);
		delete znaleziony,usuniety;
	}

	//cout << "Dodanie roznic" << endl;
	//D1.dodanieRoznic(D1,D1.wezRoot());
	//	rysuj("", "", D1.wezRoot());
	//cout << endl;
	//D1.info(D1, D1.wezRoot());


	//int licz;
	//do
	//{
	//	licz=

	//	rysuj("", "", D1.wezRoot());
	//} while (licz);
	//for (int i = 1; i <= 5; i++)
	//{
	/*if (D1.wezRoot() != NULL)
	{
	cout << endl << "Wyswietlanie";
	cout << endl << "Pre: " << endl;
	D1.Wyswietl_Pre_Order(D1.wezRoot());
	cout << endl << "Post: " << endl;
	D1.Wyswietl_Post_Order(D1.wezRoot());
	cout << endl << "In: " << endl;
	D1.Wyswietl_In_Order(D1.wezRoot());
	}
	else
	cout << "Drzewo nie istnieje" << endl;*/

		 /*******///Dodatkowe dodawanie i usuwanie elementu
					  /*if (D1.wezRoot() != NULL)
					  {
					  cout << endl << "Podaj wartosc elementu do usuniecia: ";
					  cin >> x;
					  D1.Usun(x, D1.wezRoot());
					  }
					  else
					  cout << "Nie mozna usunac";

					  char opt;
					  cout << endl << endl << "Dodac element do drzewa? (y/n): " << endl;
					  cin >> opt;
					  if (opt == 'y')
					  {
					  cin >> x;	D1.DodajElement(x, D1.wezRoot());
					  }
					  cout << endl;
					  rysuj("", "", D1.wezRoot());*/
					  //}
	return 0;
}


//Usuwanie elementu drzewa
/*void Usun(int x, dane* tmp)
{
	if (Puste())
		cout << "Drzewo jest puste" << endl;
	else
	{
		//Szukam wartosci do usuniecia
		if (x == tmp->wezWart())
		{
			if (tmp->jestZewnetrzny())// 1. Je�li nie ma synow 
			{
				if (tmp != root) // 1.1. gdy r�ny od roota
				{
					if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
						tmp->wezRodzic()->zmienPrawy(NULL);
					else
						tmp->wezRodzic()->zmienLewy(NULL);
				}
				else // 1.2. gdy root
					root = NULL;
			}
			else // 2a, 2b, i 3.
			{
				if (tmp->wezLewy() && !(tmp->wezPrawy())) // 2a. Je�li istnieje tylko lewy syn
				{
					if (tmp != root) //2a.1. gdy ro�ny od roota
					{
						if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
							tmp->wezRodzic()->zmienPrawy(tmp->wezLewy());
						else
							tmp->wezRodzic()->zmienLewy(tmp->wezLewy());
						tmp->wezLewy()->zmienRodzic(tmp->wezRodzic());
						tmp->zmienRodzic(NULL);
						tmp->zmienLewy(NULL);
					}
					else // 2a.2.gdy root
						root = tmp->wezLewy();
				}
				if (!(tmp->wezLewy()) && tmp->wezPrawy()) // 2b. Je�li istnieje tylko prawy syn
				{
					if (tmp != root) //2b.1 gdy ro�ny od roota
					{
						if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
							tmp->wezRodzic()->zmienPrawy(tmp->wezPrawy());
						else
							tmp->wezRodzic()->zmienLewy(tmp->wezPrawy());
						tmp->wezPrawy()->zmienRodzic(tmp->wezRodzic());
						tmp->zmienRodzic(NULL);
						tmp->zmienPrawy(NULL);
					}
					else // 2b.2 gdy root
						root = tmp->wezPrawy();
				}
				if (tmp->wezLewy() && tmp->wezPrawy()) // 3. Je�li prawy i lewy istnieje 
				{
					dane * tmp2 = tmp;
					tmp2 = tmp2->wezPrawy();
					if (tmp != root) // 3.1.gdy ro�ny od roota
					{
						if (!(tmp->wezPrawy()->wezLewy())) // 3.1a jesli nie istnieje lewy skrajny
						{
							tmp2->wezRodzic()->zmienPrawy(NULL);
							tmp2->zmienRodzic(tmp->wezRodzic());
							tmp2->zmienLewy(tmp->wezLewy());
							if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
								tmp->wezRodzic()->zmienPrawy(tmp2);
							else
								tmp->wezRodzic()->zmienLewy(tmp2);
							tmp->wezLewy()->zmienRodzic(tmp2);
						}
						else // 3.1b jesli istnieje lewy skrajny
						{
							while ((tmp2->wezLewy())) //przechodzimy do tego najbardziej na lewo
								tmp2 = tmp2->wezLewy();
							tmp2->wezRodzic()->zmienLewy(NULL);
							tmp2->zmienRodzic(tmp->wezRodzic());
							tmp2->zmienLewy(tmp->wezLewy());
							tmp2->zmienPrawy(tmp->wezPrawy());
							if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
								tmp->wezRodzic()->zmienPrawy(tmp2);
							else
								tmp->wezRodzic()->zmienLewy(tmp2);
							tmp->wezPrawy()->zmienRodzic(tmp2);
							tmp->wezLewy()->zmienRodzic(tmp2);
						}
					}
					else //3.2.gdy root
					{
						if (!(tmp->wezPrawy()->wezLewy())) // 3.2a jesli nie istnieje lewy skrajny
						{
							root = tmp2;
							tmp2->zmienLewy(tmp->wezLewy());
							tmp->wezLewy()->zmienRodzic(tmp2);
							tmp->zmienLewy(NULL);
						}
						else // 3.2b jesli istnieje lewy skrajny
						{
							while ((tmp2->wezLewy())) //przechodzimy do tego najbardziej na lewo
								tmp2 = tmp2->wezLewy();
							tmp2->wezRodzic()->zmienLewy(NULL);
							root = tmp2;
							tmp2->zmienLewy(tmp->wezLewy());
							tmp->wezLewy()->zmienRodzic(tmp2);
							if (tmp2->wezPrawy())
							{
								tmp2->wezPrawy()->zmienRodzic(tmp2->wezRodzic());
								tmp2->wezRodzic()->zmienLewy(tmp2->wezPrawy());
							}
							tmp2->zmienPrawy(tmp->wezPrawy());
							tmp->wezPrawy()->zmienRodzic(tmp2);
						}
					}
				}
			}
		}
		else // jesli nie znalaz�em, przechodz� po drzewie
		{
			if (x < tmp->wezWart())
				Usun(x, tmp->wezLewy());
			else
				Usun(x, tmp->wezPrawy());
		}
	}
}*/

//Sprawdzanie czy drzewo jest puste
/*bool drzewo_binarne::Puste()
{
	if (root == NULL)
		return true;
	else
		return false;
}*/

// wyswietlenie drzewa binarnego w postaci PRE - ORDER
/*void drzewo_binarne::Wyswietl_Pre_Order(dane * tmp)
{
	if (tmp)
	{
		cout << tmp->wezWart() << "   ";
		Wyswietl_Pre_Order(tmp->wezLewy());
		Wyswietl_Pre_Order(tmp->wezPrawy());
	}
}*/

// wyswietlenie drzewa binarnego w postaci POST - ORDER
/*void drzewo_binarne::Wyswietl_Post_Order(dane *tmp)
{
	if (tmp)
	{
		Wyswietl_Post_Order(tmp->wezLewy());
		Wyswietl_Post_Order(tmp->wezPrawy());
		cout << tmp->wezWart() << "   ";
	}
}*/

// wyswietlenie drzewa binarnego w postaci IN - ORDER
/*void drzewo_binarne::Wyswietl_In_Order(dane *tmp)
{
	if (tmp)
	{
		Wyswietl_In_Order(tmp->wezLewy());
		cout << tmp->wezWart() << "   ";
		Wyswietl_In_Order(tmp->wezPrawy());
	}
}*/

//Dodawanie elementu do drzewa
/*void drzewo_binarne::DodajElement(int x, dane *tmp)
{
	if (Puste())
	{
		dane* nowy = new dane;
		nowy->zmienWart(x);
		root = nowy;
		cout << "Dodano roota" << endl;
	}
	else
	{
		if (x <= tmp->wezWart())
		{
			if (tmp->wezLewy())
				DodajElement(x, tmp->wezLewy());
			else
			{
				dane* nowy = new dane;
				nowy->zmienWart(x);
				tmp->zmienLewy(nowy);
				nowy->zmienRodzic(tmp);
				cout << "Dodano lewego syna" << endl;
				
			}
		}
		else
		{
			if (tmp->wezPrawy())
				DodajElement(x, tmp->wezPrawy());
			else
			{
				dane* nowy = new dane;
				nowy->zmienWart(x);
				tmp->zmienPrawy(nowy);
				nowy->zmienRodzic(tmp);
				cout << "Dodano prawego syna" << endl;
				
			}
		}
	}

}*/

//informacje
/*int drzewo_binarne::info(drzewo_binarne D1, dane*tmp,int licz)
{
	if (licz == 1)
		return licz;
	else
	{
		dane* lewe = tmp->wezLewy();
		if (lewe)
			licz = D1.info(D1, lewe, licz);
		if (licz == 1)
			return 1;

		dane*prawe = tmp->wezPrawy();
		if (prawe)
			licz = D1.info(D1, prawe, licz);
		if (licz == 1)
			return 1;

		cout << endl << "Korzen drzewa: " << tmp->wezWart() << endl;
		if (tmp != NULL)
			cout << "Wysokosc drzewa: " << D1.Wysokosc(tmp) << endl;
		else
			cout << "Drzewo nie istnieje" << endl;

		int hl, hp, hl2, hp2;
		if (tmp->wezLewy() != NULL)
		{
			hl = D1.Wysokosc(tmp->wezLewy());
			cout << "Wysokosc lewego poddrzewa: " << hl << endl;
		}
		else
			hl = -1;
		if (tmp->wezPrawy() != NULL)
		{
			hp = D1.Wysokosc(tmp->wezPrawy());
			cout << "Wysokosc prawego poddrzewa: " << hp << endl;
		}
		else
			hp = -1;
		cout << "Roznica wysokosci: " << hl - hp << endl;

		//rotacje
		if (hl - hp == -2)
		{
			hl2 = D1.Wysokosc(tmp->wezPrawy()->wezLewy());
			hp2 = D1.Wysokosc(tmp->wezPrawy()->wezPrawy());
			if (hl2 - hp2 == 1)
			{
				rotacjaRL(D1.wezRoot(), tmp);
				D1.dodanieRoznic(D1, D1.wezRoot());
				//rysuj("", "", D1.wezRoot());
			}
			else
			{
				rotacjaRR(D1.wezRoot(), tmp);
				cout << "Dodawanie roznic" << endl;
				D1.dodanieRoznic(D1, D1.wezRoot());
				cout << "No wlasnie" << endl;
				cout << "Rootem jest: " << D1.wezRoot() << endl;
				rysuj("", "", D1.wezRoot());
			}
			licz = 1;
			return licz;
		}
		if (hl - hp == 2)
		{
			hl2 = D1.Wysokosc(tmp->wezLewy()->wezLewy());
			hp2 = D1.Wysokosc(tmp->wezLewy()->wezPrawy());
			if (hl2 - hp2 == -1)
			{
				rotacjaLR(D1.wezRoot(), tmp);
				D1.dodanieRoznic(D1, D1.wezRoot());
				
				//rysuj("", "", D1.wezRoot());
			}
			else
			{
				rotacjaLL(D1.wezRoot(), tmp);
				D1.dodanieRoznic(D1, D1.wezRoot());
				//rysuj("", "", D1.wezRoot());
			}
			licz = 1;
			return licz;
		}
	}
}*/