// drzewo.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
//drzewo binarne
#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

class dane {
	int wart;
	dane * rodzic;
	dane * lewy;
	dane * prawy;
	friend class drzewo_binarne;
public:
	dane() { zmienPrawy(0); zmienLewy(0); zmienRodzic(0); }
	int wezWart() { return wart; }
	dane *wezPrawy() { return prawy; }
	dane *wezLewy() { return lewy; }
	dane *wezRodzic() { return rodzic; }

	void zmienWart(int n_wart) { wart = n_wart; }
	void zmienPrawy(dane* n_prawy) { prawy = n_prawy; }
	void zmienLewy(dane* n_lewy) { lewy = n_lewy; }
	void zmienRodzic(dane* n_rodzic) { rodzic = n_rodzic; }
	bool jestKorzen() { return rodzic == NULL; }
	bool jestZewnetrzny() { if (lewy == NULL && prawy == NULL) return true; else return false; }
		
};

class drzewo_binarne {
	dane * root;
	public:
	drzewo_binarne() { root = NULL; }
	dane* wezRoot() { return root; }
	bool Puste();
	void DodajElement(int,dane*);
	void Usun(int,dane*);
	void Wyswietl_Pre_Order(dane *);
	void Wyswietl_Post_Order(dane *);
	void Wyswietl_In_Order(dane *);
	int Wysokosc(dane*);
	int max(int a, int b);
};

//Sprawdzanie czy drzewo jest puste
bool drzewo_binarne::Puste()
{
	if (root == NULL)
		return true;
	else
		return false;
}

//Wysokosc drzewa
int drzewo_binarne::Wysokosc(dane*tmp)
{	
	if (tmp == NULL)
		return -1;
	else
		return max( Wysokosc(tmp->wezLewy()), Wysokosc(tmp->wezPrawy())) + 1;
}

//Zwraca maks
int drzewo_binarne::max(int a, int b)
{
	if (a >= b)
		return a;
	else
		return b;
}

//Dodawanie elementu do drzewa
void drzewo_binarne::DodajElement(int x,dane *tmp)
{
	if (Puste())
	{
		dane* nowy = new dane;
		nowy->zmienWart(x);
		root = nowy;
		cout << "Dodano roota" << endl;
	}
	else
	{
		if( x <= tmp->wezWart())
		{
			if (tmp->wezLewy())
				DodajElement(x, tmp->wezLewy());
			else
			{
				dane* nowy = new dane;
				nowy->zmienWart(x);
				tmp->zmienLewy(nowy);
				nowy->zmienRodzic(tmp);
				cout << "Dodano lewego syna" << endl;
			}
		}
		else
		{
			if (tmp->wezPrawy())
				DodajElement(x, tmp->wezPrawy());
			else
			{
				dane* nowy = new dane;
				nowy->zmienWart(x);
				tmp->zmienPrawy(nowy);
				nowy->zmienRodzic(tmp);
				cout << "Dodano prawego syna" << endl;
			}
		}
	}
}

//Usuwanie elementu drzewa
void drzewo_binarne::Usun(int x, dane* tmp)
{
	if (Puste())
		cout << "Drzewo jest puste" << endl;
	else
	{
		//Szukam wartosci do usuniecia
		if (x == tmp->wezWart()) 
		{
			if ( tmp->jestZewnetrzny() )// 1. Je�li nie ma synow 
			{
				if(tmp!=root) // 1.1. gdy r�ny od roota
				{
					if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
						tmp->wezRodzic()->zmienPrawy(NULL);
					else
						tmp->wezRodzic()->zmienLewy(NULL);
				}
				else // 1.2. gdy root
					root = NULL;
			}
			else // 2a, 2b, i 3.
			{
				if ( tmp->wezLewy() && !(tmp->wezPrawy()) ) // 2a. Je�li istnieje tylko lewy syn
				{
					if (tmp != root) //2a.1. gdy ro�ny od roota
					{
						if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
							tmp->wezRodzic()->zmienPrawy(tmp->wezLewy());
						else
							tmp->wezRodzic()->zmienLewy(tmp->wezLewy());
						tmp->wezLewy()->zmienRodzic(tmp->wezRodzic());
						tmp->zmienRodzic(NULL);
						tmp->zmienLewy(NULL);
					}
					else // 2a.2.gdy root
						root = tmp->wezLewy();
				}
				if (!(tmp->wezLewy()) && tmp->wezPrawy()) // 2b. Je�li istnieje tylko prawy syn
				{
					if (tmp != root) //2b.1 gdy ro�ny od roota
					{
						if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
							tmp->wezRodzic()->zmienPrawy(tmp->wezPrawy());
						else
							tmp->wezRodzic()->zmienLewy(tmp->wezPrawy());
						tmp->wezPrawy()->zmienRodzic(tmp->wezRodzic());
						tmp->zmienRodzic(NULL);
						tmp->zmienPrawy(NULL);
					}
					else // 2b.2 gdy root
						root = tmp->wezPrawy();
				}
				if( tmp->wezLewy() && tmp->wezPrawy() ) // 3. Je�li prawy i lewy istnieje 
				{
					dane * tmp2 = tmp;
					tmp2 = tmp2->wezPrawy();
					if (tmp != root) // 3.1.gdy ro�ny od roota
					{
						if (!(tmp->wezPrawy()->wezLewy())) // 3.1a jesli nie istnieje lewy skrajny
						{
							tmp2->wezRodzic()->zmienPrawy(NULL);
							tmp2->zmienRodzic(tmp->wezRodzic());
							tmp2->zmienLewy(tmp->wezLewy());
							if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
								tmp->wezRodzic()->zmienPrawy(tmp2);
							else
								tmp->wezRodzic()->zmienLewy(tmp2);
							tmp->wezLewy()->zmienRodzic(tmp2);
						}
						else // 3.1b jesli istnieje lewy skrajny
						{
							while ((tmp2->wezLewy())) //przechodzimy do tego najbardziej na lewo
								tmp2 = tmp2->wezLewy();
							tmp2->wezRodzic()->zmienLewy(NULL);
							tmp2->zmienRodzic(tmp->wezRodzic());
							tmp2->zmienLewy(tmp->wezLewy());
							tmp2->zmienPrawy(tmp->wezPrawy());
							if (tmp->wezWart() > tmp->wezRodzic()->wezWart())
								tmp->wezRodzic()->zmienPrawy(tmp2);
							else
								tmp->wezRodzic()->zmienLewy(tmp2);
							tmp->wezPrawy()->zmienRodzic(tmp2);
							tmp->wezLewy()->zmienRodzic(tmp2);
						}
					}
					else //3.2.gdy root
					{
						if (!(tmp->wezPrawy()->wezLewy())) // 3.2a jesli nie istnieje lewy skrajny
						{
							root = tmp2;
							tmp2->zmienLewy(tmp->wezLewy());
							tmp->wezLewy()->zmienRodzic(tmp2);
							tmp->zmienLewy(NULL);
						}
						else // 3.2b jesli istnieje lewy skrajny
						{
							while ((tmp2->wezLewy())) //przechodzimy do tego najbardziej na lewo
								tmp2 = tmp2->wezLewy();
							tmp2->wezRodzic()->zmienLewy(NULL);
							root = tmp2;
							tmp2->zmienLewy(tmp->wezLewy());
							tmp->wezLewy()->zmienRodzic(tmp2);
							if (tmp2->wezPrawy())
							{
								tmp2->wezPrawy()->zmienRodzic(tmp2->wezRodzic());
								tmp2->wezRodzic()->zmienLewy(tmp2->wezPrawy());
							}
							tmp2->zmienPrawy(tmp->wezPrawy());
							tmp->wezPrawy()->zmienRodzic(tmp2);
						}
					}
				}
			}
		}
		else // jesli nie znalaz�em, przechodz� po drzewie
		{
			if (x < tmp->wezWart())
				Usun(x, tmp->wezLewy());
			else
				Usun(x, tmp->wezPrawy());
		}
	}
}

// wyswietlenie drzewa binarnego w postaci PRE - ORDER
void drzewo_binarne::Wyswietl_Pre_Order(dane * tmp)
{
	if (tmp)
	{
		cout << tmp->wezWart() << "   ";
		Wyswietl_Pre_Order(tmp->wezLewy());
		Wyswietl_Pre_Order(tmp->wezPrawy());
	}
}

// wyswietlenie drzewa binarnego w postaci POST - ORDER
void drzewo_binarne::Wyswietl_Post_Order(dane *tmp)
{
	if (tmp)
	{
		Wyswietl_Post_Order(tmp->wezLewy());
		Wyswietl_Post_Order(tmp->wezPrawy());
		cout << tmp->wezWart() << "   ";
	}
}

// wyswietlenie drzewa binarnego w postaci IN - ORDER
void drzewo_binarne::Wyswietl_In_Order(dane *tmp)
{
	if (tmp)
	{
		Wyswietl_In_Order(tmp->wezLewy());
		cout << tmp->wezWart() << "   ";
		Wyswietl_In_Order(tmp->wezPrawy());
	}
}

int main()
{
	int x;
	drzewo_binarne D1;
	for (int i = 1; i <=7; i++)
	{
		cout << "Dodaj element do drzewa: " << endl;
		cin >> x;
		D1.DodajElement(x, D1.wezRoot());
	}
	for (int i = 1; i <= 5; i++)
	{
		if (D1.wezRoot() != NULL)
		{
			cout <<endl<< "Wyswietlanie";
			cout << endl << "Pre: " << endl;
			D1.Wyswietl_Pre_Order(D1.wezRoot());
			cout << endl << "Post: " << endl;
			D1.Wyswietl_Post_Order(D1.wezRoot());
			cout << endl << "In: " << endl;
			D1.Wyswietl_In_Order(D1.wezRoot());
		}
		else
			cout << "Drzewo nie istnieje" << endl;

		cout << endl << "Korzen drzewa: " << D1.wezRoot()->wezWart() << endl;
		if (D1.wezRoot() != NULL)
			cout << "Wysokosc drzewa: " << D1.Wysokosc(D1.wezRoot()) << endl;
		else
			cout << "Drzewo nie istnieje" << endl;

		if (D1.wezRoot() != NULL)
		{
			cout << endl << "Podaj wartosc elementu do usuniecia: ";
			cin >> x;
			D1.Usun(x, D1.wezRoot());
		}
		else
			cout << "Nie mozna usunac";
		
		char opt;
		cout <<endl<<endl<< "Dodac element do drzewa? (y/n): " << endl;
		cin >> opt;
		if (opt == 'y')
		{
			cin >> x;	D1.DodajElement(x, D1.wezRoot());
		}
		cout << endl;
	}
	return 0;
}

