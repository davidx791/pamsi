// kolejka_priorytetowa.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
//kolejka priorytetowa bazujaca na sekwencji
#include "stdafx.h"
#include <iostream>
#include <string>
#include <ctime>

using namespace std;

class dane {
	int klucz;
	float wart;
	dane * nast;
	dane *poprz;
	friend class kolejka_prio;
public:
	dane() { zmienNast(0); zmienPoprz(0);}
	int wezKlucz() { return klucz; }
	float wezWart() { return wart; }
	dane *wezNast() { return nast; }
	dane *wezPoprz() { return poprz; }

	void zmienKlucz(int n_klucz) { klucz = n_klucz; }
	void zmienWart(float  n_wart) { wart = n_wart; }
	void zmienNast(dane* n_nast) { nast = n_nast; }
	void zmienPoprz(dane* n_poprz) { poprz = n_poprz; }
};

class kolejka_prio {
	dane * pocz;
	dane * koniec;
public:
	kolejka_prio() { pocz = NULL; koniec = NULL; };

	bool Pusta();
	void DodajElement(int, float);
	void UsunMax();
	void UsunMin();
	void UsunWszystko();
	void Wyswietl();
	float Min();
	float Max();
};

//Sprawdzanie czy kolejka jest pusta
bool kolejka_prio::Pusta()
{
	if (pocz == NULL)
		return true;
	else
		return false;
}

//Dodawanie elementu o kluczu k i warto�ci x
void kolejka_prio::DodajElement(int k, float x)
{
	dane *nowe = new dane;
	nowe->zmienKlucz(k);
	nowe->zmienWart(x);
	nowe->zmienNast(0);
	nowe->zmienPoprz(0);

	if (Pusta())
	{
		pocz = nowe;
		koniec = nowe;
	}
	else
	{
		//jesli priorytet pierwszego elementu jest mnniejszy od wprowadzanego elementu
		if (pocz->wezKlucz() < nowe->wezKlucz())
		{
			nowe->zmienNast(pocz);
			pocz->zmienPoprz(nowe);
			pocz = nowe;
		}
		//jesli priorytet pierwszego elementu jest wi�kszy od wprowadzanego elementu
		else
		{
			dane *tmp = pocz;
			while ((tmp->wezNast()) && (tmp->wezNast()->wezKlucz() >= nowe->wezKlucz()))
				tmp = tmp->wezNast();
			
			if (tmp->wezNast())
			{
				dane *tmp2 = tmp->wezNast();
				tmp->zmienNast(nowe);
				nowe->zmienNast(tmp2);
				tmp2->zmienPoprz(nowe);
				nowe->zmienPoprz(tmp);
			}
			else
			{
				koniec->zmienNast(nowe);
				nowe->zmienPoprz(koniec);
				koniec = nowe;
			}
		}
	}
}

//Usuwanie elementu o najwiekszym priorytecie
void kolejka_prio::UsunMax()
{
	if (Pusta())	cout << "Kolejka jest pusta" << endl;
	else
	{
		if (pocz->wezNast() != NULL)
		{
			pocz = pocz->wezNast();
			pocz->zmienPoprz(NULL);
		}
		else
			pocz = koniec = NULL;	
	}
}

//Usuwanie elementu o najmniejszym priorytecie
void kolejka_prio::UsunMin()
{
	if (Pusta())	cout << "Kolejka jest pusta" << endl;
	else
	{
		if (koniec->wezPoprz() != NULL)
		{
			koniec = koniec->wezPoprz();
			koniec->zmienNast(NULL);
		}
		else
			pocz = koniec = NULL;
	}
}

//Zwracanie elementu o najwiekszym priorytecie
float kolejka_prio::Max()
{
	if (Pusta()) return 0;
	else
	{
		dane *tmp = pocz;
		return tmp->wezWart();
	}
}

//Zwracanie elementu o najmniejszym priorytecie
float kolejka_prio::Min()
{
	if (Pusta()) return 0;
	else
	{
		dane *tmp = koniec;
		return tmp->wezWart();
	}
}

//Usuwanie kolejki
void kolejka_prio::UsunWszystko()
{
	if (!Pusta())
	{
		dane *tmp = pocz, *tmp2 = koniec;
		while (pocz->wezNast() != NULL && koniec->wezPoprz() != NULL)
		{
			if (pocz = koniec)
				break;
			pocz = pocz->wezNast();
			pocz->zmienPoprz(NULL);
			koniec = koniec->wezPoprz();
			koniec->zmienNast(NULL);
		}
		pocz = koniec = 0;
		delete tmp, tmp2;
	}
}

//Wyswietlanie zawartosci kolejki
void kolejka_prio::Wyswietl()
{
	if (Pusta())	cout << "Kolejka jest pusta" << endl;
	else
	{
		dane* tmp = pocz;
		while (tmp != NULL)
		{
		cout << tmp->wezWart() << "(" << tmp->wezKlucz() << ")   ";
		tmp = tmp->wezNast();
		}
		delete tmp;
	}
}

int main()
{
	kolejka_prio KolejkaPrio1;
	int prio;
	float wart;
	srand(time(0));
	cout << "Losowanie: " << endl;
	for (int i = 0; i < 10; i++)
	{
		prio = rand() % 50;
		wart= rand() %900+100;
		cout << "Priorytet: " << prio << " , wylosowana wartosc: " <<wart<<endl;
		KolejkaPrio1.DodajElement(prio, wart);
	}
	cout << endl<< "Zawartosc kolejki: " << endl;
	KolejkaPrio1.Wyswietl();

	cout <<endl<< "Element o najwiekszym priorytecie to:" << KolejkaPrio1.Max()<<endl;
	cout << "Element o najmniejszym priorytecie to:" << KolejkaPrio1.Min() << endl;
	cout << endl << "Usuwanie trzech elementow kolejki o najwiekszym priorytecie. Teraz kolejka wyglada tak: " << endl;
	for (int i = 0; i < 3; i++)
	{
		KolejkaPrio1.UsunMax();
	}
	KolejkaPrio1.Wyswietl();

	cout <<endl<<endl << "Usuwanie trzech elementow kolejki o najmniejszym priorytecie. Teraz kolejka wyglada tak: " << endl;
	for (int i = 0; i < 3; i++)
	{
		KolejkaPrio1.UsunMin();
	}
	KolejkaPrio1.Wyswietl();
	cout << endl << endl << "Teraz element o najwiekszym priorytecie to: "; if (KolejkaPrio1.Max()) cout<<KolejkaPrio1.Max()<<endl; else cout << endl << "Brak" << endl;
	cout << endl << "Teraz element o najmniejszym priorytecie to: ";        if (KolejkaPrio1.Min()) cout<<KolejkaPrio1.Min()<<endl; else cout << endl << "Brak" << endl;
	cout <<endl<< "Usuwanie calej kolejki";
	KolejkaPrio1.UsunWszystko();
	cout << endl;
	KolejkaPrio1.Wyswietl();
	return 0;
}
