// drzewo_ogolne.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
//drzewo ogolne
#include "stdafx.h"
#include <iostream>
#include <vector>

using namespace std;

class dane {
	int wart;
	dane * rodzic;
	friend class drzewo_ogolne;
public:
	dane() { zmienRodzic(0); }
	vector<dane*> V;
	int wezWart() { return wart; }
	dane *wezRodzic() { return rodzic; }
	void zmienWart(int n_wart) { wart = n_wart; }
	void zmienRodzic(dane* n_rodzic) { rodzic = n_rodzic; }
	bool jestKorzen() { if (rodzic == NULL) return true; else return false; }
	bool jestZewnetrzny() { if (V.empty()) return true; else return false; }
};

class drzewo_ogolne {
	dane * root;
public:
	drzewo_ogolne() { root = NULL; }
	dane* wezRoot() { return root; }
	bool Puste();
	void DodajElement(int, int, dane*);
	dane* Szukaj(int, dane*);
	void Usun(int);
	void Wyswietl_Pre_Order(dane *);
	void Wyswietl_Post_Order(dane *);
	int Wysokosc(dane*);
	int max(int a, int b);
};

//Sprawdzanie czy drzewo jest puste
bool drzewo_ogolne::Puste()
{
	if (root == NULL)
		return true;
	else
		return false;
}

//Szukanie elementu poprzedzajacego
dane* drzewo_ogolne::Szukaj(int y,dane* tmp)
{
	if (tmp->wezWart() == y)
		return tmp;
	else
	{
		dane* tmp2;
		for (int i = 0; i < tmp->V.size(); i++)
		{
			tmp2=Szukaj(y,tmp->V[i]);
			if (tmp2 != NULL)
				return tmp2;
		}
	}
	return NULL;
}
		
//Wysokosc drzewa
int drzewo_ogolne::Wysokosc(dane*tmp)
{
	if(tmp->V.empty() )
		return 0;
	else
	{
		int h = 0;
		for (int i = 0; i < tmp->V.size(); i++)
			h = max(h, Wysokosc(tmp->V[i]));
		return h + 1;
	}
}

//Zwraca maks
int drzewo_ogolne::max(int a, int b)
{
	if (a >= b)
		return a;
	else
		return b;
}

//Dodawanie elementu do drzewa
void drzewo_ogolne::DodajElement(int x, int y, dane *tmp)
{
	dane* nowy = new dane;
	if (Puste())
	{
		nowy->zmienWart(x);
		root = nowy;
		cout << "Dodano roota" << endl;
	}
	else
	{	
		tmp = Szukaj(y, tmp);
		if(tmp==root)
		{
			tmp->V.push_back(nowy);
			nowy->zmienWart(x);
			nowy->zmienRodzic(tmp);
			cout << "Dodano syna roota" << endl;
		}
		else
		{
			int z;
			cout << "Jako brat/syn [Wybierz 0/1]: " << endl;
			cin >> z;
			//Dodawanie
			if(z == 1 && tmp!=root)
			{
				tmp->V.push_back(nowy);
				nowy->zmienWart(x);
				nowy->zmienRodzic(tmp);
				cout << "Dodano syna" << endl;
			}
			if(z==0)
			{
				tmp->wezRodzic()->V.push_back(nowy);
				nowy->zmienWart(x);
				nowy->zmienRodzic(tmp->wezRodzic());
				cout << "Dodano brata" << endl;
			}
		}
	}
}

//Usuwanie wybranego elementu drzewa
void drzewo_ogolne::Usun(int x)
{
	dane* tmp = wezRoot();
	tmp=Szukaj(x, tmp);
	if (tmp->wezRodzic()==NULL ) // sekcja dotyczaca roota
	{
		if ( tmp->V.empty() ) //root nie ma synow
		{
			root = NULL;
			cout << "Drzewo puste" << endl;
		}
		else // root ma synow
		{
			if ( tmp->V.size() == 1 ) //root ma jednego syna
			{
				root = tmp->V[0];
			}
			else // root ma wiecej niz 1 syna
			{
				dane*tmp2 = tmp->V.back();
				tmp->V.pop_back();
				for (int i = 0; i < tmp->V.size();i++)
				{
					tmp2->V.push_back(tmp->V[i]);
					tmp->V[i]->zmienRodzic(tmp2);
				}
				root = tmp2;
			}
		}
	}
	else //wszystko oprocz roota
	{
		if (tmp->V.empty()) // tmp nie ma synow
		{
			for (int i = 0; i < tmp->wezRodzic()->V.size(); i++)
			{
				if (tmp->wezRodzic()->V[i] == tmp)
					tmp->wezRodzic()->V.erase(tmp->wezRodzic()->V.begin() + i);
			}
			tmp->zmienRodzic(NULL);
		}
		else // root ma synow
		{
			if (tmp->V.size() == 1) // tmp ma jednego syna
			{
				for (int i = 0; i < tmp->wezRodzic()->V.size(); i++)
				{
					if (tmp->wezRodzic()->V[i] == tmp)
						tmp->wezRodzic()->V.erase(tmp->wezRodzic()->V.begin() + i);
				}
				tmp->wezRodzic()->V.push_back(tmp->V[0]);
				tmp->zmienRodzic(NULL);
			}
			else // tmp ma wiecej niz 1 syna
			{
				dane*tmp2 = tmp->V.back();
				tmp->V.pop_back();
				for (int i = 0; i < tmp->V.size(); i++)
				{
					tmp2->V.push_back(tmp->V[i]);
					tmp->V[i]->zmienRodzic(tmp2);
				}
				for (int i = 0; i < tmp->wezRodzic()->V.size(); i++)
				{
					if (tmp->wezRodzic()->V[i] == tmp)
						tmp->wezRodzic()->V.erase(tmp->wezRodzic()->V.begin() + i);
				}
				tmp->wezRodzic()->V.push_back(tmp2);
				tmp2->zmienRodzic(tmp->wezRodzic());
				tmp->zmienRodzic(NULL);
			}
		}
	}
}

// wyswietlenie drzewa binarnego w postaci PRE - ORDER
void drzewo_ogolne::Wyswietl_Pre_Order(dane * tmp)
{
	for(int i=0;i<tmp->V.size();i++)
	{
		cout << tmp->V[i]->wezWart() <<" ";
		Wyswietl_Pre_Order(tmp->V[i]);
	}
}

// wyswietlenie drzewa binarnego w postaci POST - ORDER
void drzewo_ogolne::Wyswietl_Post_Order(dane *tmp)
{
	for(int i=0;i<tmp->V.size();i++)
	{
		Wyswietl_Post_Order(tmp->V[i]);
		cout << tmp->V[i]->wezWart() << " ";
	}
}

int main()
{
	int x, y, z;
	drzewo_ogolne D1;
	for (int i = 1; i <= 5; i++)
	{
		cout << "Dodaj element do drzewa: " << endl;
		cin >> x;
		if (!(D1.Puste())) //|| D1.wezRoot()->jestZewnetrzny() )
		{
			cout << "Za jakim elementem: " << endl;
			cin >> y;
			D1.DodajElement(x, y, D1.wezRoot());
		}
		else
		{
			y = 0;
			D1.DodajElement(x, y, D1.wezRoot());
		}

		if (D1.wezRoot() != NULL)
		{
			cout <<endl<< "Wyswietlanie" << endl<< "Pre: " << endl << D1.wezRoot()->wezWart() << " ";
			D1.Wyswietl_Pre_Order(D1.wezRoot());
			cout << endl << "Post: " << endl;
			D1.Wyswietl_Post_Order(D1.wezRoot());
			cout << D1.wezRoot()->wezWart() << endl;
		}
		cout << endl << "Korzen drzewa: " << D1.wezRoot()->wezWart() << endl;

		if (D1.wezRoot() != NULL)
			cout << "Wysokosc drzewa: " << D1.Wysokosc(D1.wezRoot()) << endl<<endl;
		else
			cout << "Drzewo nie istnieje" << endl<<endl;
	}
	for(int i=1;i<=2;i++)
	{
		if (D1.wezRoot() != NULL)
		{
			cout << endl << "Wyswietlanie" << endl << "Pre: " << endl << D1.wezRoot()->wezWart() << " ";
			D1.Wyswietl_Pre_Order(D1.wezRoot());
			cout << endl << "Post: " << endl;
			D1.Wyswietl_Post_Order(D1.wezRoot());
			cout << D1.wezRoot()->wezWart() << endl;
		}
		cout << endl << "Korzen drzewa: " << D1.wezRoot()->wezWart() << endl;

		if (D1.wezRoot() != NULL)
			cout << "Wysokosc drzewa: " << D1.Wysokosc(D1.wezRoot()) << endl;
		else
			cout << "Drzewo nie istnieje" << endl;
		
		if (D1.wezRoot() != NULL)
		{
			cout << endl << "Podaj wartosc elementu do usuniecia: " << endl;
			cin >> x;
			D1.Usun(x);
		}
		else
			cout << "Nie mozna usunac" << endl;
		cout << endl;
	}
	return 0;
}
