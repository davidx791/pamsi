﻿// zad2_lista2.cpp: Określa punkt wejścia dla aplikacji konsoli.
//

#include "stdafx.h"
#include <string>
#include <iostream>

using namespace std;

class dane {
	string imie;
	int wiek; 
	dane *nast;
	friend class lista;
public:
	dane() { zmienNast(0); }
	string wezImie() { return imie; }
	int wezWiek() { return wiek; }
	dane *wezNast() { return nast; }
		
	void zmienImie(string n_imie) { imie = n_imie; }
	void zmienWiek(int n_wiek) { wiek = n_wiek; }
	void zmienNast(dane* n_nast) { nast = n_nast; }
};

class lista {
	dane * pocz;
public:
	lista() { pocz = NULL; };
	
	void DodajElement(string,int);
	void UsunElement(int);
	void UsunWszystko();
	void Wyswietl();
};

//Dodawanie elementu na koncu listy
void lista::DodajElement(string imie, int wiek)
{
	dane *nowe = new dane;
	nowe->zmienImie(imie);
	nowe->zmienWiek(wiek);

	if (pocz == NULL)
	{
		pocz = nowe;
	}
	else
	{
		dane *tmp = pocz;
		while (tmp->wezNast())
		{
			tmp = tmp->wezNast();
		}
		tmp->zmienNast(nowe);
		nowe->zmienNast(0);
	}
}

//Usuwanie wybranego elementu listy
void lista::UsunElement(int nr)
{ 
	if(pocz ==NULL)
	{
		cout << "Lista jest pusta" << endl;
	}
	else
	{
		dane *tmp = pocz;
		if (nr == 1)
		{
			
			pocz = tmp->wezNast(); 
			cout << "Usunieto element" << endl;
		}
		if (nr >= 2)
		{
			int j = 1;
			while (tmp)
			{
				if ((j + 1) == nr)
					break;
				tmp = tmp->wezNast();
				j++;
			}

			if (tmp->wezNast()->wezNast()==0)
				tmp->zmienNast(0);
			else
				tmp->zmienNast(tmp->wezNast()->wezNast());
			cout << "Usunieto element" << endl;
		}
	}
}

//Usuwanie całej listy
void lista::UsunWszystko()
{
	if(pocz==NULL)
	{
		cout << "Lista jest pusta" << endl;
	}
	else
	{
		dane* tmp;
		while (pocz != NULL)
		{
			tmp = pocz;
			pocz = tmp->wezNast();
			delete tmp;
		}
		cout << "Usunieto" << endl;
	}
}

//Wyswietlanie zawartosci listy
void lista::Wyswietl()
{
	dane* tmp = pocz;
	while (tmp != NULL)
	{	
		cout<<tmp->wezImie()<<" "<<tmp->wezWiek()<<endl;
		tmp = tmp->wezNast();
	}
}

int main()
{
	lista *NowaLista=new lista;
	string name;
	int nr,age;
	char opcja;
	do
	{
		cout << endl << "****************MENU****************" << endl << endl;
		cout << "P. Dodaj element" << endl;
		cout << "M. Usun element" << endl;
		cout << "W. Wyswietl elementy" << endl;
		cout << "D. Usun wszystko" << endl;
		cout << "K. Zakoncz program" << endl << endl;
		cout << "Wybierz opcje: ";


		cin >> opcja;
		switch (opcja) {
		case 'P':
			cout << "Podaj imie: " << endl;
			cin >> name;
			cout << "Podaj wiek: " << endl;
			cin >> age;
			NowaLista->DodajElement(name,age);
			break;
		case 'M':
			cout << "Wprowadz nr elementu do usuniecia" << endl;
			cin >> nr;
			NowaLista->UsunElement(nr);
			if (nr <= 0)
				cout << "Bledny nr elementu" << endl;
			break;
		case 'W':
			cout << "Elementy w liscie: " << endl;
			NowaLista->Wyswietl();
			break;
		case 'D':
			NowaLista->UsunWszystko();
			break;
		case 'K':
			cout << endl << "Koniec programu" << endl;
			break;
		default:
			cout << endl << "Nieznana opcja. Wprowadz jeszcze raz" << endl << endl;
		}
	} while (opcja != 'K');
	return 0;
}

