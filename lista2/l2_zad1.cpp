#include "stdafx.h"
#include <iostream>
#include <string>
#include <ctime>
#include <cstdio>
using namespace std;

int rozm;
string * palList;

bool jestPal(const string& , int, int );
void Zamien(char &, char &);
void Perm(string, int );
void usunDup();
string * Rozszerz(string *, int);

int main()
{
	int d;
	double roznica;
	string tekst;	
	clock_t start,stop;
	do
	{
		rozm = 1;
		cout << "Wprowadz tekst: "; 
		cin >> tekst;
		start = clock();
		cout << endl;
		if (tekst == "koniec")	break;
		d = tekst.size();
		palList = new string[rozm];

		Perm(tekst,d);
		if (!palList->empty())
		{
			cout << endl << "Znaleznione palindromy: " << endl;
			for (int i = 0; i < rozm - 1; i++)
				cout << palList[i] << endl;
			
			rozm--;
			cout << endl << "Po usunieciu duplikatow: " << endl;
			usunDup();
		}
		else
			cout << "Brak palindromow" << endl;
		delete[] palList;

		cout <<endl<< "Aby zakonczyc wpisz: koniec" << endl << endl;
		stop = clock();
		roznica = stop - start;
		cout << "Czas: " << roznica <<" [ms]"<< endl;
	} while (true);
}

bool jestPal(const string& text, int i, int j)
{
	if (i < j)
	{
		if (text[i] == text[j])	jestPal(text, i + 1, j - 1);
		else	return false;
	}
	else
		return true;
}

void Zamien(char &a, char &b)
{
	char c = a;
	a = b;
	b = c;
}

void Perm(string text, int n)
{
	if (n == 1)
	{
		int d = text.size();
		if (jestPal(text, 0, d - 1)) 
			{
			palList[rozm-1] = text;
			palList=Rozszerz(palList, rozm);
			rozm++;
			}
	}
	else
	{
		for (int i = 0; i < n; i++)
		{
			Zamien(text[i], text[n - 1]);
			Perm(text, n - 1);
			Zamien(text[i], text[n - 1]);
		}
	}
}

void usunDup()//funkcja usuwajaca duplikaty z podanej tablicy
{
	int y = 0;
	string *dupli = new string[y + 1];

	for (int k = 0; k<rozm; k++)
	{
		for (int l = k; l<rozm; l++)
		{
			if ((palList[k] == palList[l]) && (l <= rozm) && (k != l))
			{
				if (palList[k] == palList[l])
				{
					palList[k] = "zero";
				}
			}
		}
	}
	int z = 0;
	for (int o = 0; o<rozm; o++)
	{
		
		if (palList[o] != "zero")
		{
			dupli[y] = palList[o];
			cout << dupli[y] << endl;
			z++;
			dupli = Rozszerz(dupli, y + 1);

		}
	}
	cout << "rozmiar: " << z << endl;

	string *tab2 = new string[y+1];//tablica pomocnicza

	for (int i = 0; i < y; i++)
		tab2[i] = dupli[i];

	delete[] dupli;
	
	for (int i = 0; i < y; i++)
		cout << tab2[i] << endl;
	
}

string * Rozszerz(string *tab1, int rozmiar)
{
	rozmiar++;
	string *tab2 = new string[rozmiar];
	for (int i = 0; i < rozmiar - 1; i++)
		tab2[i] = tab1[i];
	
	delete[]tab1;
	return tab2;
}
