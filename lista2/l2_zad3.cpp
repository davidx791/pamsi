// zad3_lista2.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <cstdlib>
using namespace std;

class dane {
	string imie;
	int wiek;
	dane *nast;
	friend class kolejka;
public:
	dane() { zmienNast(0); };

	string wezImie() { return imie; }
	int wezWiek() { return wiek; }
	dane *wezNast() { return nast; }
	
	void zmienImie(string n_imie) { imie = n_imie; }
	void zmienWiek(int n_wiek) { wiek = n_wiek; }
	void zmienNast(dane* n_nast) { nast = n_nast; }
};

class kolejka {
	dane * pocz;	
public:
	kolejka() { pocz = 0; };

	void DodajElement(string,int);
	void UsunElement();
	void UsunWszystko();
	void Wyswietl();
};

//Dodanie elementu na koniec kolejki
void kolejka::DodajElement(string imie,int wiek)
{
	dane *nowe = new dane;
	nowe->zmienImie(imie);
	nowe->zmienWiek(wiek);

	if (pocz == NULL)
	{
		pocz = nowe;
	}
	else
	{
		dane *tmp = pocz;
		while (tmp->wezNast())
		{
			tmp = tmp->wezNast();
		}
		tmp->zmienNast(nowe);
		nowe->zmienNast(0);
	}
}

//Usuwanie pierwszego elementu z kolejki 
void kolejka::UsunElement()
{
	if (pocz == NULL)
	{
		cout << "Kolejka jest pusta" << endl;
	}
	else
	{
		dane *tmp = pocz;
		pocz = tmp->wezNast();
		cout << "Usunieto element" << endl;
	}
}

//Usuwanie elementow z kolejki
void kolejka::UsunWszystko()
{
	dane* tmp;
	while (pocz != NULL)
	{
		tmp = pocz;
		pocz = tmp->wezNast();
		delete tmp;
		cout << "Usunieto" << endl;
	}
	if (pocz == NULL)
		cout << "Kolejka jest pusta" << endl;
}

//Wyswietlanie calej kolejki
void kolejka::Wyswietl()
{
	dane* tmp = pocz;
	cout << "Zawartosc kolejki:" << endl;
	while (tmp != NULL)
	{
		cout << tmp->wezImie()<<" , "<<tmp->wezWiek()<< endl;
		tmp = tmp->wezNast();
	}
}

int main()
{
	kolejka *NowaKolejka = new kolejka;
	string name;
	int age;
	char opcja;
	do
	{
		cout << endl << "****************MENU****************" << endl << endl;
		cout << "P. Dodaj element" << endl;
		cout << "M. Usun element" << endl;
		cout << "W. Wyswietl elementy" << endl;
		cout << "D. Usun wszystko" << endl;
		cout << "K. Zakoncz program" << endl << endl;
		cout << "Wybierz opcje: ";

		cin >> opcja;
		switch (opcja) {
		case 'P':
			cout << "Podaj imie: " << endl;			cin >> name;
			cout << "Podaj wiek: " << endl;			cin >> age;
			NowaKolejka->DodajElement(name,age);
			break;
		case 'M':
			NowaKolejka->UsunElement();
			break;
		case 'W':
			NowaKolejka->Wyswietl();
			break;
		case 'D':
			NowaKolejka->UsunWszystko();
			break;
		case 'K':
			cout << endl << "Koniec programu" << endl;
			break;
		default:
			cout << endl << "Nieznana opcja. Wprowadz jeszcze raz" << endl << endl;
		}
	} while (opcja != 'K');
	return 0;
}