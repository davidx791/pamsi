// zad4_lista2.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//

#include "stdafx.h"
#include <string>
#include <iostream>

using namespace std;

class dane {
	string napis;
	dane *nast;
	dane *poprz;
	friend class lista;
public:
	dane() { zmienNast(0); zmienPoprz(0); }
	string wezNapis() { return napis; }
	dane *wezNast() { return nast; }
	dane *wezPoprz() { return poprz; }

	void zmienNapis(string n_napis) { napis = n_napis; }
	void zmienNast(dane* n_nast) { nast = n_nast; }
	void zmienPoprz(dane* n_poprz) { poprz = n_poprz; }
};

class deque {
	int licz;
	dane * pocz;
	dane * koniec;
public:
	deque() { pocz = NULL;	koniec = NULL; licz = 0;};

	int rozmiar();
	bool jestPusta();
	void DodajPierwszy(string);
	void DodajOstatni(string);
	void UsunPierwszy();
	void UsunOstatni();
	void UsunWszystko();
	void WyswietlPocz();
	void WyswietlKoniec();
	void Pal();
};

//Dodawanie elementu na poczatku deque
void deque::DodajPierwszy(string napis)
{
	dane *tmp = new dane;
	tmp->zmienNapis(napis);
	tmp->zmienNast(0);
	tmp->zmienPoprz(0);
	
	if (jestPusta())
	{
		pocz = tmp;
		koniec = tmp;
	}
	else
	{
		tmp->zmienNast(pocz);
		pocz->zmienPoprz(tmp);
		pocz = tmp;
	}
	licz++;
}

//Dodawanie elementu na koncu deque
void deque::DodajOstatni(string napis)
{
	dane *tmp = new dane;
	tmp->zmienNapis(napis);
	tmp->zmienNast(0);
	tmp->zmienPoprz(0);

	if (jestPusta())
	{
		pocz = tmp;
		koniec = tmp;
	}
	else
	{
		koniec->zmienNast(tmp);
		tmp->zmienPoprz(koniec);
		koniec = tmp;
	}
	licz++;
}

//Usuwanie pierwszego elementu z deque
void deque::UsunPierwszy()
{
	if (jestPusta())
		cout<<"Deque jest pusta"<<endl;
	else
	{
		dane* tmp = pocz;
		if (pocz->wezNast() != NULL)
		{
			pocz = pocz->wezNast();
			pocz->zmienPoprz(NULL);
		}
		else
		{
			pocz = NULL;
		}
		delete tmp;
		licz--;
	}
}

//Usuwanie ostatniego elementu z deque
void deque::UsunOstatni()
{
	if (jestPusta())
		cout << "Deque jest pusta" << endl;
	else
	{
		dane* tmp = koniec;
		if (koniec->wezPoprz() != NULL)
		{
			koniec = koniec->wezPoprz();
			koniec->zmienNast(NULL);
		}
		else
		{
			koniec = NULL;
		}
		delete tmp;
		licz--;
	}
}

//Usuwanie ca�ej deque
void deque::UsunWszystko()
{
	if (jestPusta())
		cout << "Deque jest pusta" << endl;
	else
	{
		dane* tmp = pocz,*tmp2 =koniec;
		while (pocz->wezNast() != NULL && koniec->wezPoprz() != NULL)
		{
			if (pocz == koniec)
				break;
			else
			{
				pocz = pocz->wezNast();
				pocz->zmienPoprz(NULL);
				koniec = koniec->wezPoprz();
				koniec->zmienNast(NULL);
			}					
		}
		pocz = 0;
		koniec = 0;
		licz = 0;
		delete tmp,tmp2;
	}
}


//Wyswietlanie zawartosci deque od poczatku
void deque::WyswietlPocz()
{
	if (jestPusta())
		cout << "Deque pusta" << endl;
	else
	{
		dane* tmp = pocz;
		cout << "Elementy w liscie wyswietlane od poczatku: " << endl;
		while (tmp != NULL)
		{
			cout << tmp->wezNapis();
			tmp = tmp->wezNast();
		}
		delete tmp;
	}
}
//Wyswietlanie zawartosci deque od konca
void deque::WyswietlKoniec()
{
	if (jestPusta())
		cout << "Deque pusta" << endl;
	else
	{
		dane* tmp = koniec;
		cout << "Elementy w liscie wyswietlane od konca: " << endl;
		while (tmp != NULL)
		{
			cout << tmp->wezNapis();
			tmp = tmp->wezPoprz();
		}
		delete tmp;
	}
}

int deque::rozmiar()
{
	return licz;
}

bool deque::jestPusta()
{
	if (licz == 0)
		return true;
	else
		return false;
}

void deque::Pal()
{
	if (jestPusta())
		cout << "Deque jest pusta" << endl;
	else
	{
		int j = 0;
		dane *tmp =pocz;
		dane *tmp2=koniec;
		for (int i = 0; i < (rozmiar()/2); i++)
		{
			if (tmp->wezNapis() == tmp2->wezNapis())
				j++;
			tmp = tmp->wezNast();
			tmp2 = tmp2->wezPoprz();
		}
		
		if ((rozmiar()/2) == j)
			cout << "Wyraz jest palindromem" << endl;
		else
			cout << "Wyraz nie jest palindromem" << endl;
		//delete tmp, tmp2;
	}
}

int main()
{
	deque *NowaDeque = new deque;
	string text;
	char opcja;
	do
	{
		cout << endl << "****************MENU****************" << endl << endl;
		cout << "1. Dodaj element na poczatek" << endl;
		cout << "2. Dodaj element na koniec" << endl;
		cout << "3. Usun element z poczatku" << endl;
		cout << "4. Usun element z konca" << endl;
		cout << "5. Wyswietl elementy od poczatku" << endl;
		cout << "6. Wyswietl elementy od konca" << endl;
		cout << "7. Usun wszystko" << endl;
		cout << "8. Pokaz rozmiar" << endl;
		cout << "9. Sprawdzanie palindromu" << endl;
		cout << "K. Zakoncz program" << endl << endl;
		cout << "Wybierz opcje: ";


		cin >> opcja;
		switch (opcja) {
		case '1':
			cout << "Podaj tekst: " << endl;
			cin >> text;
			NowaDeque->DodajPierwszy(text);
			break;
		case '2':
			cout << "Podaj tekst: " << endl;
			cin >> text;
			NowaDeque->DodajOstatni(text);
			break;
		case '3':
			NowaDeque->UsunPierwszy();
			break;
		case '4':
			NowaDeque->UsunOstatni();
			break;
		case '5':
			NowaDeque->WyswietlPocz();
			break;
		case '6':
			NowaDeque->WyswietlKoniec();
			break;
		case '7':
			NowaDeque->UsunWszystko();
			break;
		case '8':
			cout << "Rozmiar deque: "<< NowaDeque->rozmiar() << endl;;
			break;
		case '9':
			cout << endl << "Sprawdzanie palindromu: " << endl;
			NowaDeque->Pal();
			break;
		case 'K':
			cout << endl << "Koniec programu" << endl;
			break;
		default:
			cout << endl << "Nieznana opcja. Wprowadz jeszcze raz" << endl << endl;
		}
	} while (opcja != 'K');
	return 0;
}
