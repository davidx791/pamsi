// sort.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
#include "stdafx.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <cstdio>
using namespace std;
int *tab,*tabpom;
int *utworzTablice(double param,int n);
void scalanie(int tab1[], int pocz, int sr, int kon);
void mergesort(int tab1[], int pocz, int kon);

int podziel(int tabq[], int lewy, int prawy);
void quicksort(int tabq[], int lewy, int prawy);


void heapify(int tabh[], int rozmiar, int indeks);
void heapsort(int tabh[], int licz);

void introsort(int tabi[], int l, int p, int max);
void intro(int tabi[], int l, int n);

int main()
{
	srand(time(NULL));
	double roznica;
	clock_t start, stop;
	char opcja;
	cout << endl << "Podaj rozmiar tablicy: ";
	int n = 250000;
	tab = new int[n];
	tabpom = new int[n];
	cout << endl << "**********************************************" << endl;
	cout << "1 - Losowa tablica" << endl;
	cout << "2 - 25% poczatkowych elementow posortowane" << endl;
	cout << "3 - 50% poczatkowych elementow posortowane" << endl;
	cout << "4 - 75% poczatkowych elementow posortowane" << endl;
	cout << "5 - 95% poczatkowych elementow posortowane" << endl;
	cout << "6 - 99% poczatkowych elementow posortowane" << endl;
	cout << "7 - 99,7% poczatkowych elementow posortowane" << endl;
	cout << "8 - odwrotnie posortowana tablica" << endl;
	cout << "Wybierz: ";
	cin >> opcja;
	switch (opcja)
	{
	case '1':
		tab = utworzTablice(0,n);
		break;
	case '2':
		tab = utworzTablice(0.25,n);
		break;
	case '3':
		tab = utworzTablice(0.4,n);
		break;
	case '4':
		tab = utworzTablice(0.75,n);
		break;
	case '5':
		tab = utworzTablice(0.95,n);
		break;
	case '6':
		tab = utworzTablice(0.99,n);
		break;
	case '7':
		tab = utworzTablice(0.997,n);
		break;
	case '8':
		tab = utworzTablice(1,n);
		break;
	default:
		cout << endl << "Nieznana opcja." << endl << endl;
	}

	for (int i = 0.8*n; i<n; i++)
		cout << tab[i] << " ";

	start = clock();
	cout << "test1" << endl;

	//mergesort(tab,0, n - 1);
	quicksort(tab, 0, n - 1);
	//intro(tab, -1, n);

	cout << "test2" << endl;
	stop = clock();
	
	for (int i = 0.8*n; i<n; i++)
		cout << tab[i] << " ";

	roznica = (stop - start);
	cout << endl << "Czas: " << roznica << " [ms]" << endl;
	return 0;
}

//Losowanie z zakresu 1-10000
int *utworzTablice(double param,int n)
{
	if (param == 0 || param == 1)
	{
		if (param == 0) //losowa tablica
			for (int i = 0; i < n; i++)
				tab[i] = rand() % 10000 + 1;//	cout << i << "." << tab[i] << endl;
		if (param == 1)//posortowane odwrotnie
		{
			int * tab2 = new int[n];
			for (int i = 0; i<n; i++)//losowanie 
				tab2[i] = rand() % 10000 + 1;
			mergesort(tab2, 0, n - 1);//sortowanie
			for (int i = 0; i < n; i++)//utworzenie posortowanej odwrotnie
			{
				tab[i] = tab2[n - 1 - i]; //cout << i << "." << tab[i] << " ";
			}
		}
	}
	else
	{
		int * tab2 = new int[n*param];
		for (int i = 0; i<n*param; i++)//losowanie 1 cz�ci
			tab2[i] = rand() % 10000 + 1;
		mergesort(tab2, 0, n*param - 1);//sortowanie
		for (int i = 0; i<n*param; i++)//posortowanie 1 cz�ci
			tab[i] = tab2[i];//cout << i << "." << tab[i] << endl;
		for (int i = n*param; i < n; i++) // losowanie kolejnych
			tab[i] = rand() % 10000 + 1;//cout << i << "." << tab[i] << endl;
	}
	return tab;
}

// Scalanie dwoch posortowanych ciagow
void scalanie(int  tab1[], int pocz, int sr, int kon)
{
	int i, j, q;
	for (i = pocz; i <= kon; i++) tabpom[i] = tab1[i];  // Skopiowanie danych do tablicy pomocniczej
	i = pocz; j = sr + 1; q = pocz;                 // Ustawienie wska�nik�w tablic
	while (i <= sr && j <= kon) {    // Przenoszenie danych z sortowaniem ze zbior�w pomocniczych do tablicy g��wnej
		if (tabpom[i]<tabpom[j])
			tab1[q++] = tabpom[i++];
		else
			tab1[q++] = tabpom[j++];
	}
	while (i <= sr) tab1[q++] = tabpom[i++]; // Przeniesienie nie skopiowanych danych ze zbioru pierwszego w przypadku, gdy drugi zbi�r si� sko�czy�
}

//Sortowanie przez scalanie
void mergesort(int tab1[], int pocz, int kon)
{
	if (pocz<kon)
	{
		int sr = (pocz + kon) / 2;
		mergesort(tab1, pocz, sr);    // Dzielenie lewej cz�ci
		mergesort(tab1, sr + 1, kon);   // Dzielenie prawej cz�ci
		scalanie(tab1, pocz, sr, kon);   // ��czenie cz�ci lewej i prawej
	}
}

//Podzia� tablicy na dwie cz�ci, w pierwszej liczby mniejsze badz rowne x, w drugiej wieksze
int podziel(int tabq[], int lewy, int prawy) 
{
	int piwot = tabq[lewy]; 
	int i = lewy, j = prawy, temp; // i, j - indeksy w tabeli
	while (true) 
	{
		while (tabq[j] > piwot) j--;// dopoki elementy sa wieksze od piwota
		while (tabq[i] < piwot) i++; // dopoki elementy sa mniejsze od piwota
		if (i < j) // zamieniamy miejscami gdy i < j
		{
			temp = tabq[i];
			tabq[i] = tabq[j];
			tabq[j] = temp;
			i++;
			j--;
		}
		else // gdy i >= j zwracamy j jako punkt podzialu tablicy
			return j;
	}
}

//Sortowanie - quicksort
void quicksort(int tabq[], int lewy, int prawy) // sortowanie szybkie
{
	int pkt;
	if (lewy < prawy)
	{
		pkt = podziel(tabq, lewy, prawy); // podzia� tablicy na dwie cz�ci
		quicksort(tabq, lewy, pkt); // rekurencyjny quicksort dla pierwszej czesci tablicy
		quicksort(tabq, pkt + 1, prawy); // rekurencyjny quicksort dla drugiej czesci tablicy
	}
}

//Poprawianie kopca
void heapify(int tabh[], int rozmiar, int indeks)
{
	int lewy = (indeks + 1) * 2 - 1;
	int prawy = (indeks + 1) * 2;
	int x = 0;

	if (lewy < rozmiar && tabh[lewy] > tabh[indeks])
		x = lewy;
	else
		x = indeks;

	if (prawy < rozmiar && tabh[prawy] > tabh[x])
		x = prawy;

	if (x != indeks)
	{
		int temp = tabh[indeks];
		tabh[indeks] = tabh[x];
		tabh[x] = temp;

		heapify(tabh, rozmiar, x);
	}
}

//Sortowanie przez kopcowanie
void heapsort(int tabh[], int licz) 
{
	int rozmiarkopca = licz;
	for (int j = (rozmiarkopca - 1) / 2; j >= 0; --j)//budowanie kopca
		heapify(tabh, rozmiarkopca, j);

	for (int i = licz - 1; i > 0; --i)//rozbieranie kopca z sortowaniem
	{
		int temp = tabh[i];
		tabh[i] = tabh[0];
		tabh[0] = temp;

		--rozmiarkopca;
		heapify(tabh, rozmiarkopca, 0);
	}
}

//Sortowanie untrospektywne
void introsort(int tab[], int l, int p, int max)
{
	if (p <= 1)
		return;
	if (max = 0)// gdy zostanie przekroczona maksymalna glebokosc
	{
		heapsort(tab, p); //wywo�anie heapsorta
		return;
	}
	if (l < p)//w przeciwnym razie quicksort 
	{
		int pkt = podziel(tab, l, p); //dzielenie z quicksorta
		introsort(tab, l, pkt, max - 1);
		introsort(tab, pkt + 1, p, max - 1);
	}
}

//wywo�anie sortowania introspektywnego z maxem
void intro(int tabh[], int l, int n)
{
	int max = (int)floor(2 * log(n));
	introsort(tabh, l, n - 1, max);
}
