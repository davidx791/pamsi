#include "stdafx.h"
#include<iostream>
#include<cstdlib>
#include<ctime>

using namespace std;

int **Losuj(int x,int w, int k);
void Wyswietl(int w, int k, int **tab);
void Max(int w, int k, int **tab);

int main()
{
	int wiersze=0, kolumny=0,x=0;
	char opcja;
	int **t;
	srand(time(0));
	do
	{
		cout << endl << "****************MENU****************" << endl << endl;
		cout << "L. Wylosuj liczby i utworz tablice" << endl;
		cout << "W. Wyswietl tablice" << endl;
		cout << "M. Znajdz maksymalna liczbe w tablicy" << endl;
		cout << "K. Zakoncz program" << endl << endl;
		cout << "Wybierz opcje: ";
		
		
		cin >> opcja;
		switch (opcja) {
		case 'L':
			cout << endl << "Podaj liczbe wierszy: ";
			cin >> wiersze;
			while (cin.fail() || wiersze<=0) { cin.clear(); cin.ignore(10000, '\n'); cout << "Wprowadz poprawnie" << endl; cin >> wiersze; }
			cout << endl << "Podaj liczbe kolumn: ";
			cin >> kolumny;
			while (cin.fail() || kolumny<=0) { cin.clear(); cin.ignore(10000, '\n'); cout << "Wprowadz poprawnie" << endl; cin >> kolumny; }
			cout << endl << "Podaj maksymalna liczbe wieksza od 0 jaka moze zostac wylosowana: ";
			cin >> x;
			while (cin.fail() || x<=0) { cin.clear(); cin.ignore(10000, '\n'); cout << "Wprowadz jeszcze raz" << endl; cin >> x; }
			t = Losuj(x,wiersze, kolumny);
			break;
		case 'W':
			if(wiersze==0 && kolumny==0 && x==0){cout << "Najpierw wylosuj liczby" << endl; break;}
			Wyswietl(wiersze, kolumny, t);
			break;
		case 'M':
			if (wiersze == 0 && kolumny == 0 && x == 0) { cout << "Najpierw wylosuj liczby" << endl; break; }
			Max(wiersze,kolumny,t);
			break;
		case 'K':
			cout <<endl<< "Koniec programu" << endl;
			break;
		default:
			cout << endl << "Nieznana opcja. Wprowadz jeszcze raz" << endl << endl;
		}
	} while (opcja != 'K');
	return 0;
}

int **Losuj(int x,int w, int k)
{
	int **tab = new int *[w];
	for (int i = 0; i < w; i++)
	{
		tab[i] = new int[k];
		for (int j = 0; j < k; j++)
		tab[i][j] = rand() % (x+1);
	}
	return tab;
}

void Wyswietl(int w, int k, int** tab)
{
	for (int i = 0; i < w; i++)
	{
		for (int j = 0; j < k; j++)
			cout << tab[i][j] << " ";
		cout << endl;
	}
}

void Max(int w, int k, int**tab)
{
	int max = 0;
	for (int i = 0; i < w; i++)
		for (int j = 0; j < k; j++)
			if (max <= tab[i][j]) max = tab[i][j];
	cout << "Maksymalna liczba to: " << max << endl;
}
