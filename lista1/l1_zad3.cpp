// zad3_lista1.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//
#include "stdafx.h"
#include <iostream>

using namespace std;

int Potega(int, int);
int Silnia(int);

int main()
{
	int p,x,y;
	char opcja;
	do
	{
		cout << "..........Menu.........." << endl;
		cout << "P. Oblicz potege" << endl;
		cout << "S. Oblicz silnie" << endl;
		cout << "Z. Zakoncz" << endl;

		cout << "Wybierz: " << endl;
		cin >> opcja;
		switch (opcja)
		{
		case 'P':
			cout << "Podaj podstawe" << endl;
			cin >> x;
			while (!cin.good()) { cin.clear(); cin.ignore(1000, '\n'); cout << "Wprowadz poprawne dane" << endl; cin >> x; }
			cout << "Podaj wykladnik" << endl;
			cin >> p;
			while (!cin.good()) { cin.clear(); cin.ignore(1000, '\n'); cout << "Wprowadz poprawne dane" << endl; cin >> p; }
			cout << x << " do potegi " << p << " to " << Potega(x, p) << endl;
			break;
		case 'S':
			cout << "Podaj liczbe naturalna" << endl;
			cin >> y;
			while (y < 0 || !cin.good()) { cin.clear(); cin.ignore(1000, '\n'); cout << "Podaj jeszcze raz: "; cin >> y; }
			cout << y << "! " << " to " << Silnia(y) << endl;
			break;
		case 'Z':
			cout << "Koniec programu" << endl;
			break;
		default: cout << "Nieznana opcja. Wprowadz jeszcze raz: " << endl;
		}
	} while (opcja != 'Z');
	
    return 0;
}

int Potega(int X, int P)
{
	if (P >= 1)
		return X*Potega(X, P - 1);
	if(P==0 || P==1)
		return 1;
}

int Silnia(int Y)
{
	if (Y==0 || Y==1)
		return 1;
	return Y*Silnia(Y - 1);
}
