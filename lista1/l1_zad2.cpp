// zad2_lista1.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <ctime>
#include <string>
#include <cstdio>
#include <cstdlib>

using namespace std;

void ZapiszT(string,int);
void WczytajT(string);
void ZapiszB(string,int);
void WczytajB(string,int);
int main()
{
	int n;
	char opcja;
	string nazwa,nazwa2,nazwa3,nazwa4;
	cout << endl << "Podaj dlugosc tablicy" << endl;
	cin >> n;
	do
	{
	cout << endl << "**************MENU****************" << endl << endl;
	cout << "1. Zapisz tablice jednowymiarowa do pliku tekstowego" << endl;
	cout << "2. Wczytaj tablice jednowymiarowa z pliku tekstowego" << endl;
	cout << "3. Zapisz tablice jednowymiarowa do pliku binarnego" << endl;
	cout << "4. Wczytaj tablice jednowymiarowa z pliku binarnego" << endl;
	cout << "5. Zakoncz program" << endl << endl;

	
	cout << "Wybierz numer opcji: ";

	cin >> opcja;
	switch (opcja)
	{
	case '1':
		cout<<endl << "Podaj nazwe pliku: ";
		cin >> nazwa;
		while (n <= 0 || !cin.good()) { cin.clear(); cin.ignore(1000, '\n'); cout << "Wprowadz jeszcze raz: "; cin >> n; }
		ZapiszT(nazwa,n);
		cout << "Wylosowano " << n << " liczb zakresu 0-100 i zapisano do pliku o nazwie " << nazwa << endl;
		break;
	case '2':
		cout << endl << "Podaj nazwe pliku do otwarcia wraz z rozszezrzeniem: ";
		cin >> nazwa2;
		cout << endl << "Zawartosc pliku to: " << endl;
		WczytajT(nazwa2);
		break;
	case '3':
		cout << endl << "Podaj nazwe pliku: ";
		cin >> nazwa3;
		while (n <= 0 || !cin.good()) { cin.clear(); cin.ignore(1000, '\n'); cout << "Wprowadz jeszcze raz: "; cin >> n; }
		ZapiszB(nazwa3, n);
		cout <<endl<<"Wylosowano " << n << " liczb zakresu 0-100 i zapisano do pliku o nazwie " << nazwa3 << endl;
		break;
	case '4':
		cout << endl << "Podaj nazwe pliku do otwarcia wraz z rozszezrzeniem: ";
		cin >> nazwa4;
		cout << endl << "Zawartosc pliku to: " << endl;
		WczytajB(nazwa4,n);
		break;
	case '5':
		cout << "Koniec programu" << endl;
		break;
	default:
		cout << endl << "Nieznana opcja. Wprowadz cyfre jeszcze raz" << endl << endl;
	}
	} while (opcja != '5');
	return 0;
}

void ZapiszT(string name,int n)
{
	int * tab = new int [n];
	srand(time(0));
	ofstream plik_wyj;
	plik_wyj.open(name);
	for (int i = 0; i < n; i++) 
		{
		tab[i] = rand() % 101;
		plik_wyj << tab[i] << " ";
		}
	plik_wyj.close();
}

void WczytajT(string name2)
{
	ifstream plik_wej;
	string dane;
	plik_wej.open(name2);
	while(!plik_wej.eof() )
	{
		plik_wej >> dane;
		cout << dane << endl;
	}
	cout << endl << "Koniec pliku o nazwie " << name2 << endl;
	plik_wej.close();

}

void ZapiszB(string name3,int m)
{
	int * tab = new int[m];
	srand(time(0));
	ofstream plikB_wyj;
	plikB_wyj.open(name3,ios::binary);
		for (int i = 0; i < m; i++)
	{
		tab[i] = rand() % 101;
		cout << tab[i] << " " ;
		plikB_wyj.write((const char*)&tab[i],sizeof tab[i]);
	}
	plikB_wyj.close();
}

void WczytajB(string name4,int x)
{
	int * tab2 = new int[x];
	ifstream plikB_wej;
	plikB_wej.open(name4, ios::binary || ios::in);
	for (int i = 0; i < x; i++) {
		plikB_wej.read((char*)&tab2[i], sizeof tab2[i]);
		cout << tab2[i] << " ";
	}
		
	cout << endl << "Koniec pliku o nazwie " << name4 << endl;
	plikB_wej.close();
}