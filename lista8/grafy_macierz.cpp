// grafy_macierz.cpp: Okre�la punkt wej�cia dla aplikacji konsoli.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <vector>
#include <string>
#include <ctime>
#include<stack>

using namespace std;

const int MAXINT = -2147483647;
int n, m; //liczba wierzcholkow, krawedzi
float gest;
int ** macierz;
string litery = "ABCDEFGHIJKLMNOPRSTUVWYZ";
class krawedz;
class wierzcholek;
class slistEl2;
class stos;
class slistEl;
class queue;
vector<string>los;
vector<wierzcholek>listaWierzcholkow(n); //lista wierzcholkow
vector<krawedz>listaKrawedzi(m); //lista krawedzi

void prim();
void kruskal();
bool cykl(int **A, wierzcholek a);
int spojnosc();
void dodajKrawedz();
//void usunKrawedz(krawedz tmp);
wierzcholek dodajWierzcholek(krawedz a);
wierzcholek dodajWierzcholekRecznie(krawedz a);
//void usunWierzcholek(wierzcholek a);
//int wyszukajWierzcholek(wierzcholek tmp);
//oid zastapWierzcholek(wierzcholek a, wierzcholek tmp);
//void wierzcholkiKoncowe(krawedz tmp);
wierzcholek przeciwleglyWierzcholek(krawedz tmp, wierzcholek pom);
//void zastapKrawedz(krawedz a, krawedz tmp);
//bool saSasiednie(wierzcholek a, wierzcholek b);
void krawedzie();
void wierzcholki();
int bylaWylosowana(string liczba, int i, vector<string> tab);
string losujNazwe(string litery, vector<string>los);

class wierzcholek
{
public:
	string nazwa;
	int indeks;
	string ety_w;
	vector<krawedz> listaIncydencji;
	friend class krawedz;
};

class krawedz
{
public:
	krawedz() { v1.indeks = -1; }
	int waga;
	wierzcholek v1, v2;
	string ety_k;
	int indeks2;
	friend class wierzcholek;
};

class slistEl
{
public:	
	slistEl * next;
	int prio;
	krawedz data;
};

class queue
{
private:
	slistEl * head;
	slistEl * tail;

public:
	queue();      // konstruktor
	~queue();     // destruktor
	bool empty(void);
	krawedz Min(void);
	int  frontprio(void);
	void DodajElement(int prio, krawedz v);
	void UsunMin(void);
};

class slistEl2
{
public:
  slistEl2 * next;
  wierzcholek v;
};

class stos
{
  private:
    slistEl2 * S;   // lista przechowuj�ca stos

  public:
    stos();       // konstruktor
    ~stos();      // destruktor
    bool empty(void);
	wierzcholek top(void);
    void push(wierzcholek v);
    void pop(void);
};

/*
class dane {
	int klucz;
	krawedz wart;
	dane * nast;
	dane *poprz;
	friend class kolejka_prio;
public:
	dane() { zmienNast(0); zmienPoprz(0); }
	int wezKlucz() { return klucz; }
	krawedz wezWart() { return wart; }
	dane *wezNast() { return nast; }
	dane *wezPoprz() { return poprz; }

	void zmienKlucz(int n_klucz) { klucz = n_klucz; }
	void zmienWart(krawedz n_wart) { wart = n_wart; }
	void zmienNast(dane* n_nast) { nast = n_nast; }
	void zmienPoprz(dane* n_poprz) { poprz = n_poprz; }
};

class kolejka_prio {
	dane * pocz;
	dane * koniec;
public:
	kolejka_prio() { pocz = NULL; koniec = NULL; };

	bool Pusta();
	void DodajElement(int, krawedz);
	void UsunMax();
	void UsunMin();
	void UsunWszystko();
	krawedz Min();
	krawedz Max();
};
*/
void dodajKrawedz()
{
	krawedz tmp;
	tmp.waga = rand() % 90 + 10; // losowanie z przedzia�u 10-99
	tmp.v1 = dodajWierzcholek(tmp);
	tmp.v2 = dodajWierzcholek(tmp);
	tmp.indeks2 = listaKrawedzi.size();
	listaKrawedzi.push_back(tmp);
	macierz[tmp.v1.indeks][tmp.v2.indeks] = macierz[tmp.v2.indeks][tmp.v1.indeks] = tmp.waga;
}

wierzcholek dodajWierzcholek(krawedz a)
{
	wierzcholek tmp;
	if (listaWierzcholkow.size() != n)
	{
		tmp.nazwa = losujNazwe(litery, los);
		tmp.indeks = listaWierzcholkow.size();
		listaWierzcholkow.push_back(tmp);
		return tmp;
	}
	else
	{
		if (a.v1.indeks == -1)
		{
			//for (int i = 0; i < listaWierzcholkow.size(); i++)
			//	for (int j = i + 1; j < listaWierzcholkow.size(); j++)
			while (true)
			{
				int i = rand() % listaWierzcholkow.size();
				int j = rand() % listaWierzcholkow.size();
				//if (macierz[i][j] == 0 && i != j)
				if(macierz[i][j] == 0)
				{
					if (i != j)
					{
						tmp.indeks = i;
						tmp.nazwa = listaWierzcholkow[tmp.indeks].nazwa;
						return tmp;
					}
				}
			}
		
		}
		else
		{
			
			while (true)
			{
				//for (int i = 0; i < listaWierzcholkow.size(); i++)
				int i = rand() % listaWierzcholkow.size();
			//	if (listaWierzcholkow[i].indeks != a.v1.indeks && macierz[a.v1.indeks][i] == 0)
				if (listaWierzcholkow[i].indeks != a.v1.indeks)
				{
					if (macierz[a.v1.indeks][i] == 0)
					{
						tmp.indeks = i;
						tmp.nazwa = listaWierzcholkow[tmp.indeks].nazwa;
						return tmp;
					}
				}
			}
		}
	}
}

wierzcholek dodajWierzcholekRecznie(krawedz a)
{
	wierzcholek tmp;
	if (listaWierzcholkow.size() != n)
	{
		cout << "Podaj nazwe wierzcholka: " << endl;
		cin >> tmp.nazwa;
		tmp.indeks = listaWierzcholkow.size();
		listaWierzcholkow.push_back(tmp);
		return tmp;
	}
	else
	{
			cout << "Podaj indeks utworzonego wierzcholka" << endl;
			cin>>tmp.indeks;
			tmp.nazwa = listaWierzcholkow[tmp.indeks].nazwa;
			return tmp;
	}
}

wierzcholek przeciwleglyWierzcholek(krawedz tmp, wierzcholek pom)
{
	if (pom.indeks == tmp.v1.indeks)
	{
		cout << tmp.v2.nazwa << "(" << tmp.v2.indeks << ")" << endl;
		return tmp.v2;
	}
	else
	{
		cout << tmp.v1.nazwa << "(" << tmp.v1.indeks << ")" << endl;
		return tmp.v1;
	}
}

void krawedzie()
{
	cout << "Krawedzie: " << endl;
	for (int i = 0; i<listaKrawedzi.size(); i++)
		cout << "Krawedz: " << listaKrawedzi[i].waga << "[" << listaKrawedzi[i].indeks2
		<< "]  " << "( " << listaKrawedzi[i].v1.nazwa << "[" << listaKrawedzi[i].v1.indeks
		<< "]   " << " -> " << listaKrawedzi[i].v2.nazwa << "[" << listaKrawedzi[i].v2.indeks
		<< "]   " << " )" << endl;
}
void wierzcholki()
{
	cout << "Wierzcholki: " << endl;
	for (int i = 0; i < listaWierzcholkow.size(); i++)
	{
		cout << "Wierzcholek: " << listaWierzcholkow[i].nazwa << "["
			<< listaWierzcholkow[i].indeks << "]" << endl;
	}
}

int bylaWylosowana(string liczba, int i, vector<string> tab)
{
	int j;
	for (j = 0; j <i; j++)
		if (tab[j] == liczba)
			return true;
	return false;
}

string losujNazwe(string litery, vector<string>los)
{
	string tmp;
	int a = rand() % 24;
	int b = rand() % 24;
	int c = rand() % 24;
	tmp = litery[a];
	tmp = tmp + litery[b];
	tmp = tmp + litery[c];
	los.push_back(tmp);
	if (!los.empty())
	{
		while (bylaWylosowana(tmp, los.size() - 1, los))
		{
			int a = rand() % 24;
			int b = rand() % 24;
			int c = rand() % 24;
			tmp = litery[a];
			tmp = tmp + litery[b];
			tmp = tmp + litery[c];
			los.push_back(tmp);
		}
	}
	return tmp;
}

void prim()
{
	clock_t start, stop;
	start = clock();
	//kolejka_prio Q;
	queue Q;
	vector<krawedz> T;
	bool * visited = new bool[n];
	for (int i = 0; i < n; i++)
		visited[i] = false;
	
	//int x = rand() % listaWierzcholkow.size();
	//wierzcholek v = listaWierzcholkow[x]; //losowy wierzcholek
	//visited[listaWierzcholkow[x].indeks] = true;
	wierzcholek v = listaWierzcholkow[0];
	visited[listaWierzcholkow[0].indeks] = true;
	
	for (int i = 1; i < n; i++)//Do drzewa dodaje n-1 krawedzi grafu
	{
		krawedz tmp;
		for (int j = 0; j < listaWierzcholkow.size(); j++)
		{
			//if (v.indeks != j && macierz[v.indeks][j] != 0)
			if (macierz[v.indeks][j] != 0)
			{
				if (v.indeks != j)
				{
					if ((visited[j]) == false)
					{
						tmp.v1 = v;
						tmp.v2 = listaWierzcholkow[j];
						tmp.waga = macierz[v.indeks][j];
						Q.DodajElement(tmp.waga, tmp);
					}
				}
			}
		}
		do
		{
			tmp = Q.Min();
			Q.UsunMin();
		} while (visited[tmp.v2.indeks]);

		T.push_back(tmp);
		visited[tmp.v2.indeks] = true;
		v = tmp.v2;
	}
	stop = clock();
	Q.~queue();
	delete[]visited;

	
	int suma = 0;
	for (int i = 0; i < T.size(); i++)
	{
		cout << "Krawedz: " << T[i].waga << "   " << T[i].v1.nazwa << "[" << T[i].v1.indeks << "] -> " << T[i].v2.nazwa << "[" << T[i].v2.indeks << "]" << endl;
		suma = suma + T[i].waga;
	}
	cout << "Suma wag prim: " << suma << endl;
	cout << endl << "Czas PRIMA: " << stop - start << " [ms]" << endl << endl;
	T.clear();
}

void kruskal()
{
	clock_t start, stop;
	start = clock();
	vector<krawedz> T; // pusty zbior krawedzi T
	//vector<wierzcholek> pom;
	
	//Tworzymy kolejke priorytetowa i wrzucamy do niej wszystkie krawedzie
	//kolejka_prio Q;
	queue Q;
	for (int i = 0; i < listaKrawedzi.size(); i++)
		Q.DodajElement(listaKrawedzi[i].waga,listaKrawedzi[i]);                    // i umieszczamy je w kolejce priorytetowej
	
	//Tworzymy macierz pomocnicza i wype�niamy zerami
	int ** macierzpom=new int * [n];
	for (int i = 0; i < n; i++)
		macierzpom[i] = new int[n];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++) 
			macierzpom[i][j] = 0;
		
	krawedz tmp;
	tmp = Q.Min();
	//cout << "Q.min: " << tmp.waga;
	Q.UsunMin();
	macierzpom[tmp.v1.indeks][tmp.v2.indeks] = tmp.waga;
	macierzpom[tmp.v2.indeks][tmp.v1.indeks] = tmp.waga;
	T.push_back(tmp);                 // Dodajemy kraw�d� do drzewa
	//cout <<endl<<"Dodawanie krawedzi 0: "<<tmp.waga << endl;

	for (int i = 1; i < n-1; i++)    // P�tla wykonuje si� n - 2 razy 
	{
		int k;
		do
		{
			k = 0;
			tmp = Q.Min();              // Pobieramy z kolejki kraw�d� o najmniejszej wadze
			//cout << "Q.min: " << tmp.waga << endl;
			Q.UsunMin();                    // Kraw�d� usuwamy z kolejki
			macierzpom[tmp.v1.indeks][tmp.v2.indeks] = tmp.waga;
			macierzpom[tmp.v2.indeks][tmp.v1.indeks] = tmp.waga;
			if (cykl(macierzpom, tmp.v1) )
			{
				macierzpom[tmp.v1.indeks][tmp.v2.indeks] = 0;
				macierzpom[tmp.v2.indeks][tmp.v1.indeks] = 0;
				k = 1;
			}
		} while (k);
		T.push_back(tmp);                 // Dodajemy kraw�d� do drzewa
		//cout << "Dodawanie krawedzi "<<i<<": "<<tmp.waga<< endl;
	}
	stop = clock();
	for (int i = 0; i < n; i++) delete[] macierzpom[i];
		delete[] macierzpom;
	Q.~queue();
	
	

	int suma = 0;
	for (int i = 0; i < T.size(); i++)
	{
		cout << "Krawedz: " << T[i].waga << "   " << T[i].v1.nazwa << "[" << T[i].v1.indeks << "] -> " << T[i].v2.nazwa << "[" << T[i].v2.indeks << "]" << endl;
		suma = suma + T[i].waga;
	}
	cout << "Suma wag kruskal: " << suma << endl;
	cout << endl << "Czas KRUSKALA: " << stop - start << " [ms]" << endl;
	T.clear();
}

bool cykl(int **A, wierzcholek a)
{
	//stack<wierzcholek> S;          // Stos
	stos S;
	bool * visited2;               // Tablica odwiedzin
	wierzcholek w, v;
	int z;                  // Zmienne pomocnicze

	visited2 = new bool[n];        // Tworzymy tablic� odwiedzin
	for (int i = 0; i < n; i++)
		visited2[i] = false;  // i zerujemy j�

	wierzcholek pom;
	pom.indeks = -1;
	pom.nazwa = "pomoc";
	S.push(listaWierzcholkow[a.indeks]); S.push(pom);        // Na stos wierzcho�ek startowy i -1
	visited2[a.indeks] = true;            // Oznaczamy wierzcho�ek jako odwiedzony
	while (!S.empty())             // W p�tli przechodzimy graf za pomoc� DFS
	{

		w = S.top(); S.pop();       // Pobieramy ze stosu wierzcho�ek z kt�rego przyszli�my
		v = S.top(); S.pop();       // oraz wierzcho�ek bie��cy
		//cout << "Pobieramy ze stosu: " << w.indeks << " " << v.indeks << endl;
		for (int j = 0; j < listaWierzcholkow.size(); j++) // przegladamy liste sasiadaow
		{
			//if (v.indeks != j && A[v.indeks][j] != 0)
			if (A[v.indeks][j] != 0)
			{
				if (v.indeks != j)
				{
					z = j;                // Numer s�siada
					//cout << "Wyswietlam nr sasiada " << j << endl;
					if (!visited2[z])
					{
						S.push(listaWierzcholkow[z]); S.push(v);   // S�siada nieodwiedzonego umieszczamy na stosie
						visited2[z] = true;      // Oznaczamy go jako odwiedzonego
					}
					else
					{
						if (z != w.indeks)           // Je�li s�siad jest odwiedzony i nie jest wierzcho�kiem
						{                         // z kt�rego przyszli�my, to odkryli�my cykl
							//cout << "Odkrylismy cykl" << endl;
							S.~stos();
							delete[] visited2;      // Usuwamy zmienne pomocnicze
							return true;            // Ko�czymy z wynikiem true
						}
					}
				}
			}
		}
	}
	delete[] visited2;            // W grafie nie ma cykli.
	S.~stos();
	//cout << "W grafie nie ma cykli" << endl;
	return false;                 // Ko�czymy z wynikiem false
}

int spojnosc()
{
	stos S2;
	bool *visited = new bool[n];
	
	for (int i = 0; i < n; i++)
		visited[i] = false;

	//Badamy sp�jno�� grafu
	int licz = 0;                    // Zerujemy licznik wierzcho�k�w
	S2.push(listaWierzcholkow[0]);                 // Wierzcho�ek startowy na stos
	visited[listaWierzcholkow[0].indeks] = true;         // Oznaczamy go jako odwiedzony

	while (!S2.empty())          // Wykonujemy przej�cie DFS
	{
		wierzcholek v = S2.top();             // Pobieramy wierzcho�ek ze stosu
		S2.pop();                 // Pobrany wierzcho�ek usuwamy ze stosu
		licz++;                    // Zwi�kszamy licznik wierzcho�k�w
		for (int i = 0; i < listaWierzcholkow.size(); i++) // Przegl�damy s�siad�w
		{
			//if (listaWierzcholkow[i].indeks != v.indeks && macierz[v.indeks][i] != 0)
			if (macierz[v.indeks][i] != 0)
			{
				if (listaWierzcholkow[i].indeks != v.indeks && macierz[v.indeks][i] != 0)
				{
					//wierzcholek u = p->v;
					if (!visited[listaWierzcholkow[i].indeks])        // Szukamy wierzcho�k�w nieodwiedzonych
					{
						visited[listaWierzcholkow[i].indeks] = true;   // Oznaczamy wierzcho�ek jako odwiedzony
						S2.push(listaWierzcholkow[i]);           // i umieszczamy go na stosie
					}
				}
			}
		}
	}
	S2.~stos();
	delete[] visited;
	return licz;
}

/*
//Sprawdzanie czy kolejka jest pusta
bool kolejka_prio::Pusta()
{
	if (pocz == NULL)
		return true;
	else
		return false;
}

//Dodawanie elementu o kluczu k i warto�ci x
void kolejka_prio::DodajElement(int k, krawedz x)
{
	dane *nowe = new dane;
	nowe->zmienKlucz(k);
	nowe->zmienWart(x);
	nowe->zmienNast(0);
	nowe->zmienPoprz(0);

	if (Pusta())
	{
		pocz = nowe;
		koniec = nowe;
	}
	else
	{
		//jesli priorytet pierwszego elementu jest mnniejszy od wprowadzanego elementu
		if (pocz->wezKlucz() < nowe->wezKlucz())
		{
			nowe->zmienNast(pocz);
			pocz->zmienPoprz(nowe);
			pocz = nowe;
		}
		//jesli priorytet pierwszego elementu jest wi�kszy od wprowadzanego elementu
		else
		{
			dane *tmp = pocz;
			while ((tmp->wezNast()) && (tmp->wezNast()->wezKlucz() >= nowe->wezKlucz()))
				tmp = tmp->wezNast();

			if (tmp->wezNast())
			{
				dane *tmp2 = tmp->wezNast();
				tmp->zmienNast(nowe);
				nowe->zmienNast(tmp2);
				tmp2->zmienPoprz(nowe);
				nowe->zmienPoprz(tmp);
			}
			else
			{
				koniec->zmienNast(nowe);
				nowe->zmienPoprz(koniec);
				koniec = nowe;
			}
		}
	}
}

//Usuwanie elementu o najwiekszym priorytecie
void kolejka_prio::UsunMax()
{
	if (Pusta())	cout << "Kolejka jest pusta" << endl;
	else
	{
		if (pocz->wezNast() != NULL)
		{
			pocz = pocz->wezNast();
			pocz->zmienPoprz(NULL);
		}
		else
			pocz = koniec = NULL;
	}
}

//Usuwanie elementu o najmniejszym priorytecie
void kolejka_prio::UsunMin()
{
	if (Pusta())	cout << "Kolejka jest pusta" << endl;
	else
	{
		if (koniec->wezPoprz() != NULL)
		{
			koniec = koniec->wezPoprz();
			koniec->zmienNast(NULL);
		}
		else
			pocz = koniec = NULL;
	}
}

//Zwracanie elementu o najwiekszym priorytecie
krawedz kolejka_prio::Max()
{
	//if (Pusta()) return 0;
	//else
	//{
		dane *tmp = pocz;
		return tmp->wezWart();
	//}
}

//Zwracanie elementu o najmniejszym priorytecie
krawedz kolejka_prio::Min()
{
	//if (Pusta()) return 0;
	//else
	//{
		dane *tmp = koniec;
		return tmp->wezWart();
	//}
}

//Usuwanie kolejki
void kolejka_prio::UsunWszystko()
{
	if (!Pusta())
	{
		dane *tmp = pocz, *tmp2 = koniec;
		while (pocz->wezNast() != NULL && koniec->wezPoprz() != NULL)
		{
			if (pocz = koniec)
				break;
			pocz = pocz->wezNast();
			pocz->zmienPoprz(NULL);
			koniec = koniec->wezPoprz();
			koniec->zmienNast(NULL);
		}
		pocz = koniec = 0;
		delete tmp, tmp2;
	}
}
*/

queue::queue()
{
	head = tail = NULL;
}

queue::~queue()
{
	while (head) UsunMin();
}

bool queue::empty(void)
{
	return !head;
}

krawedz queue::Min(void)
{
	//if (head)
		return head->data;
	//else     return -MAXINT;
}

int queue::frontprio(void)
{
	if (!head) return -MAXINT;
	else      return head->prio;
}

void queue::DodajElement(int prio, krawedz v)
{
	slistEl * p, *r;
	p = new slistEl;
	p->next = NULL;
	p->prio = prio;
	p->data = v;

	if (!head) head = tail = p;
	else if (head->prio > prio)
	{
		p->next = head;
		head = p;
	}
	else
	{
		r = head;
		while ((r->next) && (r->next->prio <= prio))
			r = r->next;
		p->next = r->next;
		r->next = p;
		if (!p->next) tail = p;
	}
}

void queue::UsunMin(void)
{
	if (head)
	{
		slistEl * p = head;
		head = head->next;
		if (!head) tail = NULL;
		delete p;
	}
}

stos::stos()
{
	S = NULL;
}

stos::~stos()
{
	while (S) pop();
}

bool stos::empty(void)
{
	return !S;
}

wierzcholek stos::top(void)
{
	return S->v;
}

void stos::push(wierzcholek v)
{
	slistEl2 * e = new slistEl2;
	e->v = v;
	e->next = S;
	S = e;
}

void stos::pop(void)
{
	if (S)
	{
		slistEl2 * e = S;
		S = S->next;
		delete e;
	}
}

int main()
{
	srand(time(NULL));
	clock_t start, stop;
		
	cout << "Podaj liczbe wierzcholkow i gestosc grafu: " << endl;
	cin >> n >> gest; //6, 0.6
	//n = 50;
	//gest =0.5;
	//if (gest == 1)
		m = gest*n*(n - 1) / 2; //liczba krawedzi
	//else
	//	m = gest*n*(n - 1) / 2 + 1; //liczba krawedzi
	cout << "Liczba wierzcholkow: " << n << endl;
	cout << "Gestosc: " << gest << endl;
	cout << "Liczba krawedzi: " << m << endl;

	macierz = new int *[n]; // Tworzymy tablic� wska�nik�w
	for (int i = 0; i < n; i++)
		macierz[i] = new int[n]; // Tworzymy wiersze

	// Macierz wype�niamy zerami
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++) macierz[i][j] = 0;

	//DODAJ KRAWEDZIE LOSOWO
	/*start = clock();
	for (int i = 0; i < m; i++)
	{
		dodajKrawedz();
	}
	stop = clock();*/
	//cout << endl << "Czas dodawania krawedzi: " << stop - start << " [ms]" << endl;
	
	//DODAJ KRAWEDZIE RECZNIE
	start = clock();
	cout << "Poczatkowo podac nowe wierzcholki (bez powtorzen)" << endl;
	for (int i = 0; i < m; i++)
	{
		krawedz tmp;
		cout << "Podaj wage: ";
		cin>>tmp.waga;
		tmp.v1 = dodajWierzcholekRecznie(tmp);
		tmp.v2 = dodajWierzcholekRecznie(tmp);
		tmp.indeks2 = listaKrawedzi.size();
		listaKrawedzi.push_back(tmp);
		macierz[tmp.v1.indeks][tmp.v2.indeks] = macierz[tmp.v2.indeks][tmp.v1.indeks] = tmp.waga;
	}
	stop = clock();
	//cout << endl << "Czas dodawania krawedzi: " << stop - start << " [ms]" << endl;
	
	
	//WYSWIETLANIE KRAWEDZI
	krawedzie();
	wierzcholki();

	// Wypisujemy zawarto�� macierzy s�siedztwa
	/*cout << "   ";
	for (int i = 0; i < n; i++) cout << setw(3) << i;
	cout << endl << endl;
	for (int i = 0; i < n; i++)
	{
		cout << setw(3) << i;
		for (int j = 0; j < n; j++) cout << setw(3) << (int)macierz[i][j];
		cout << endl;
	}*/

	//JESLI SPOJNY TO PRIM I KRUSKAL
	if (spojnosc() == n)
	{
		cout << "Spojny" << endl;
		cout << "Poczatek prima" << endl;
		prim();
		cout << "Poczatek kruskala" << endl;
		kruskal();
	}
	else
		cout<< "Niespojny" << endl;


	system("pause");
	
	cout << endl;
	return 0;
}
